# -*- coding: utf-8 -*-
import datetime
from course.models import PublicElectiveCourseSet, PublicElectiveCourse, PublicCompulsoryCourseSet, \
    PublicCompulsoryCourse, SpecializedCourseSet, SpecializedCourse
from models import Student
from aliyun import AliyunAPI


def clear_all_data():
    """
    用来在学期之间删除数据的
    :return:
    """
    Student.circulate_selection_courses.through.objects.all().delete()
    Student.selection_fail_courses.through.objects.all().delete()
    Student.liked_public_elective_courses.through.objects.all().delete()


def check_if_need_send_sms():
    """
    检查是否需要发送短信通知
    请在每天选课处理之前检查
    :return:
    """
    Student.objects.all().update(need_send_sms=True)
    for student in Student.objects.filter(sms_service=True):
        try:
            # 为了防止没获取到课表，加三次重试好了
            for i in range(3):
                error_code, success = student.check_if_need_send_sms(force=True)
                if success:
                    break
        except:
            pass


def send_public_elective_selection_sms_notice():
    """
    发短信通知
    请确保在循环选课之前取回结果
    :return:
    """
    for student in Student.objects.filter(sms_service=True, need_send_sms=True):
        try:
            # 为了防止没获取到课表，加三次重试好了
            for i in range(3):
                error_code, success = student.send_message_of_public_elective_selection_result()
                if success:
                    break
        except:
            print student.number


def circulate_select_go():
    """
    循环选课的函数
    :return:
    """
    i = 0
    print datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'w') as f:
        f.write('')
    for student in Student.objects.exclude(circulate_selection_courses=None):
        try:
            result, success = student.submit_all_circulate_selection_course()
            if not success:
                with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'a') as f:
                    f.write('%s\n' % student.number)
        except Exception:
            with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'a') as f:
                f.write('%s\n' % student.number)
        i += 1
        print i

    with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'r') as f:
        failed_list = f.readlines()
    i = 0
    while failed_list:
        i += 1
        count = len(failed_list)
        reselect()
        with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'r') as f:
            failed_list = f.readlines()
        if len(failed_list) == count:
            break
        if i > 30:
            break


def reselect():
    print datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'r') as f:
        failed_list = f.readlines()
    with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'w') as f:
        f.write('')
    print 'total' + str(len(failed_list))
    i = 0
    for student_number in failed_list:
        if student_number:
            student = Student.objects.get(number=student_number.strip('\n'))
            try:
                result, success = student.submit_all_circulate_selection_course()
                if not success:
                    with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'a') as f:
                        f.write('%s\n' % student.number)
            except Exception:
                with open(datetime.date.today().strftime('%Y%m%d_failed_list.txt'), 'a') as f:
                    f.write('%s\n' % student.number)
            i += 1
            print i


def send_public_elective_season_end_notice_to_everyone():
    for student in Student.objects.filter(contact_number__isnull=False):
        AliyunAPI.send_public_elective_season_end_notice(student=student)