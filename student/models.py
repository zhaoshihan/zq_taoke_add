# -*- coding: utf-8 -*-
"""
student model方法说明
model方法尽量封装了所有可能的表查询操作和对api的调用，以保证封装的完整性

model方法的返回值也尽量统一化

对于重载的方法：
    保持原有的接口
对于自定义的方法：
    if 方法不会出现可预见的错误:
        if 有返回值:
            return data
        else:  # 即无返回值
            return 'success'
    else:  # 即有可能出现的错误
        if 成功:
            if 有返回值：
                return data, True
            else:  # 即没有返回值
                return 0, True
        else:  # 即失败
            return error_code, True

每个model方法必须写注释！！！
"""
from functools import wraps

import logging
import datetime
import time
import os
import random
import threading

from django.db import models
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.cache import cache

from course.models import PublicElectiveCourse, PublicElectiveCourseSet, SpecializedCourse, SpecializedCourseSet, \
    PublicCompulsoryCourse, PublicCompulsoryCourseSet, Academy, CourseBase
from api import API
from api.APIErrors import EduPwdError
from suggestion.models import PopUpNotice, StudentNoticeRelations
from aliyun import AliyunAPI

import zq_taoke_rebuild.settings as settings

logger = logging.getLogger('database_info')


def refresh_public_elective_selection_result(method):
    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(student, force=None):
        """
        从API更新选课列表，处理失败的课程和成功的课程，返回待处理的课程详情
        对于选课成功的课程从循环选课中删除
        指定force不为None即可强制更新
        :param student:
        :param force:
        :return:
        """
        result, success = API.get_public_elective_course_selection_result(student)
        if success:
            failed_courses_numbers = []
            success_courses_numbers = []
            selecting_courses_numbers = []
            if force:
                for lesson in result:
                    if lesson['status'].startswith(u'失败'):
                        failed_courses_numbers.append(lesson['identifier'])
                    elif lesson['status'].startswith(u'成功'):
                        success_courses_numbers.append(lesson['identifier'])
                    elif lesson['status'].startswith(u'待处理'):
                        selecting_courses_numbers.append(lesson['identifier'])
                failed_courses = PublicElectiveCourse.objects.filter(number__in=failed_courses_numbers)
                success_courses = PublicElectiveCourse.objects.filter(number__in=success_courses_numbers)
                selecting_courses = PublicElectiveCourse.objects.filter(number__in=selecting_courses_numbers)
                return method(student, failed_courses=failed_courses, success_courses=success_courses,
                              selecting_courses=selecting_courses)
            elif student.selection_result_update_time != datetime.date.today() \
                    and datetime.datetime.today().hour >= \
                    settings.COURSE_SEASON_DATE['PUBLIC_ELECTIVE_COURSE']['start_hour']:
                # 每天第一次去刷新选课结果，避免多次发信息通知
                student.selection_result_update_time = datetime.date.today()
                student.save()
                for lesson in result:
                    if lesson['status'].startswith(u'失败'):
                        failed_courses_numbers.append(lesson['identifier'])
                    elif lesson['status'].startswith(u'成功'):
                        success_courses_numbers.append(lesson['identifier'])
                    elif lesson['status'].startswith(u'待处理'):
                        selecting_courses_numbers.append(lesson['identifier'])
                failed_courses = PublicElectiveCourse.objects.filter(number__in=failed_courses_numbers)
                success_courses = PublicElectiveCourse.objects.filter(number__in=success_courses_numbers)
                selecting_courses = PublicElectiveCourse.objects.filter(number__in=selecting_courses_numbers)
                if failed_courses:
                    # 这句话会先执行一次DELETE然后再把后面QuerySet里面的对象INSERT INTO一下
                    student.selection_fail_courses = failed_courses
                if success_courses:
                    # 这里success_courses为空或者success_courses中的course不在circulate_selection_courses中都不会引发错误
                    student.circulate_selection_courses.remove(*success_courses)
                return method(student, failed_courses=failed_courses, success_courses=success_courses,
                              selecting_courses=selecting_courses)
            else:
                for lesson in result:
                    if lesson['status'].startswith(u'待处理'):
                        selecting_courses_numbers.append(lesson['identifier'])
                failed_courses = PublicElectiveCourse.objects.filter(number__in=failed_courses_numbers)
                success_courses = PublicElectiveCourse.objects.filter(number__in=success_courses_numbers)
                selecting_courses = PublicElectiveCourse.objects.filter(number__in=selecting_courses_numbers)
                return method(student, failed_courses=failed_courses, success_courses=success_courses,
                              selecting_courses=selecting_courses)
        else:
            logger.error(u'API Error %s, failed: student %s refresh selection fail courses' % (result, student.number))
            return result, False

    return wrapper


# 暂时会造成问题
# def student_data_cache(method):
#     # 此wraps装饰器是快速传递函数的元数据的
#     @wraps(method)
#     def wrapper(student, force=None):
#         cache_key = '%s_%s' % (student.number, method.func_name)
#         if not force:
#             data = cache.get(cache_key, None)
#             if data:
#                 return data
#         # 如果是force或者是取缓存为空则进行查询
#         data = method(student)
#         cache.set(cache_key, data, None)
#         return data
#
#     return wrapper


# 检查是否为选课季时间布尔值，两者按照else语句是否需要定制按需取用
is_public_elective_course_season = False
if settings.COURSE_SEASON_DATE['PUBLIC_ELECTIVE_COURSE']['from'] < time.localtime() < \
        settings.COURSE_SEASON_DATE['PUBLIC_ELECTIVE_COURSE']['to']:
    is_public_elective_course_season = True


# 检查是否为选课季时间装饰器，两者按照else语句是否需要定制按需取用
def must_be_in_the_public_elective_course_season(method):
    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(*args, **kwargs):
        if settings.COURSE_SEASON_DATE['PUBLIC_ELECTIVE_COURSE']['from'] < time.localtime() < \
                settings.COURSE_SEASON_DATE['PUBLIC_ELECTIVE_COURSE']['to']:
            return method(*args, **kwargs)
        else:
            # not in public elective season
            return 1, False

    return wrapper


def get_current_week():
    """
    :return: x, 当前是第x周
    """
    # 教务系统的一周的开始是周一
    first_sunday_date = settings.SEMESTER_DATE['FIRST_MONDAY']
    delta_days = (datetime.date.today() - first_sunday_date).days
    if delta_days < 0:
        return 1
    else:
        week_count = delta_days / 7 + 1
        if week_count <= 20:
            return week_count
        else:
            return 20


def check_if_courses_need_update(method):
    """
    装饰器，用来检查是否需要更新课表，并执行必要的更新
    :param method:
    :return:
    """

    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(student, force=None):
        if student.course_table_update_time == datetime.date.today() and not force:
            pass
        else:
            t = threading.Thread(target=student.update_student_course)
            t.setDaemon(False)
            t.start()
        return method(student)

    return wrapper

# Create your models here.


class Student(models.Model):
    GENDER_CHOICE = (
        (u'未知', u'未知'),
        (u'男', u'男'),
        (u'女', u'女'),
    )

    user = models.OneToOneField(to=User)
    number = models.BigIntegerField(verbose_name=u'学生学号', db_index=True)
    name = models.CharField(verbose_name=u'学生姓名', max_length=20)
    password = models.CharField(verbose_name=u'教务系统密码', max_length=30, blank=True, null=True)
    gender = models.CharField(verbose_name=u'性别', choices=GENDER_CHOICE, default=u'未知', max_length=4)
    gpa = models.FloatField(verbose_name=u'GPA', default=0.0, blank=True, null=True)
    liked_public_elective_courses = models.ManyToManyField(verbose_name=u'收藏的公选课程', to=PublicElectiveCourse,
                                                           related_name=u'student_like_this_course', blank=True)
    sign_in_time = models.DateTimeField(verbose_name=u'第一次登陆时间', auto_now_add=True)
    sys_avatar = models.IntegerField(verbose_name=u'用户系统头像用户名', default=1)
    avatar = models.CharField(verbose_name=u'用户头像的文件名', max_length=50, blank=True, null=True)
    # False表示系统头像，True表示自定头像
    avatar_type_switch = models.BooleanField(verbose_name=u'用户的头像类型', default=False)
    academy = models.ForeignKey(verbose_name=u'所属学院', to=Academy, blank=True, null=True)
    token = models.CharField(verbose_name=u'掌武api对应token', max_length=50, blank=True, null=True)
    rf_token = models.CharField(verbose_name=u'掌武api对应refresh_token', max_length=50, blank=True, null=True)
    # todo: token expire in 字段和相关的更新、使用逻辑
    selection_fail_courses = models.ManyToManyField(verbose_name=u'循环选课失败列表', to=PublicElectiveCourse,
                                                    related_name=u'student_failed_to_select', blank=True)
    circulate_selection_courses = models.ManyToManyField(verbose_name=u'循环选课列表', to=PublicElectiveCourse,
                                                         related_name=u'student_select_this_course', blank=True)
    selection_result_update_time = models.DateField(verbose_name=u'选课列表更新时间', blank=True, null=True)
    course_table_update_time = models.DateField(verbose_name=u'课表更新时间', blank=True, null=True)
    selection_list_submit_time = models.DateField(verbose_name=u'选课列表提交时间', blank=True, null=True)
    contact_number = models.BigIntegerField(verbose_name=u'联系手机号', blank=True, null=True)
    sms_service = models.BooleanField(verbose_name=u'是否开通短信提醒', default=False)
    # 解决在不选课了之后重复发送选课成功的短信
    need_send_sms = models.BooleanField(verbose_name=u'是否需要发送短信', default=True)

    # 取出该用户所有评论 可用 (Student_instance).comment_set.all() 获得

    class Meta:
        verbose_name = u'学生'
        verbose_name_plural = u'学生'

    def __unicode__(self):
        return u'%s%s' % (self.number, self.name)

    def random_sys_avatar(self):
        if self.avatar_type_switch:
            # 如果原来是自定义头像，修改头像模式并且删除原来的头像文件
            self.avatar_type_switch = False
            if self.avatar:
                try:
                    os.remove(os.path.join(os.getcwd(), 'media', 'img', 'hand_img', '%s' % self.avatar))
                except Exception:
                    pass
        # 不要随机到原来的头像
        random_int = random.randint(1, 40)
        while random_int == self.sys_avatar:
            random_int = random.randint(1, 40)
        self.sys_avatar = random_int
        self.save()
        return '/static/img/hand_img/default/%s.png' % random_int

    def get_avatar(self):
        if self.avatar_type_switch:
            return "/media/img/hand_img/%s" % self.avatar
        else:
            return "/static/img/hand_img/default/%s.png" % self.sys_avatar

    @staticmethod
    def get_avail_path(dir_local, dir_relative, name):
        path = dir_relative + name
        while os.path.exists(dir_local + path):
            name = str(hash(''.join(random.sample('qwertyuioplkjhgfdsazxcvbnm1234567890', 10)))) + name
            path = dir_relative + name
        return path, name

    def avatar_file_save(self, f):
        if not self.avatar_type_switch:
            self.avatar_type_switch = True
        path, name = Student.get_avail_path(os.path.dirname(os.path.dirname(__file__)).replace('\\', '/') + '/',
                                            'media/img/hand_img/', f.name)
        try:
            dest = open(os.path.dirname(os.path.dirname(__file__)).replace('\\', '/') + '/' + path, 'wb+')
        except IOError:
            os.makedirs('media//img//hand_img')
            dest = open(os.path.dirname(os.path.dirname(__file__)).replace('\\', '/') + '/' + path, 'wb+')
        for chunk in f.chunks():
            dest.write(chunk)
        dest.close()
        if self.avatar:
            try:
                os.remove(os.path.join(os.getcwd(), 'media', 'img', 'hand_img', '%s' % self.avatar))
            except Exception:
                pass
        self.avatar = name
        self.save()
        return "/media/img/hand_img/%s" % self.avatar

    def get_student_abstract(self):
        """
        如果这里的查询的字段变动，请查看对这个方法的调用时做的prefetch是否也需要做相应的变动
        :return:
        """
        data = {
            'name': self.name,
            'gender': self.gender,
            'avatar': self.get_avatar(),
        }
        return data

    def get_student_detail(self, force=None):
        """
        只允许对单个student对象调用，多个对象迭代调用请用get_student_abstract()方法
        :return: dic 学生的详细信息
        """
        # 解决一下历史遗留的学生信息缺失的问题
        if not force:
            if not self.name or not self.academy or not self.academy.name.strip():
                result, success = API.get_information(self)
                if success:
                    if not self.name:
                        self.name = result['name']
                        if not result['college']:
                            # 防止每次都进去获取
                            result['college'] = '未知'
                        academy = Academy.objects.get_or_create(name=result['college'])[0]
                        self.academy = academy
                        self.save()
            if not self.gender:
                result, success = API.get_gender(self)
                if success:
                    self.gender = result['gender']
                    self.save()
        else:
            result, success = API.get_information(self)
            if success:
                self.name = result['name']
                if not result['college']:
                    # 防止每次都进去获取
                    result['college'] = '未知'
                academy = Academy.objects.get_or_create(name=result['college'])[0]
                self.academy = academy
                self.save()
            result, success = API.get_gender(self)
            if success:
                self.gender = result['gender']
                self.save()
        data = {
            'name': self.name,
            'gender': self.gender,
            'avatar': self.get_avatar(),
            'gpa': self.get_gpa()[0],
            'academy': self.academy.name,
            'has_used_sms': self.sms_service,
        }
        return data

    # @student_data_cache
    def get_liked_courses(self):
        """
        根据
        获取收藏课程列表
        :return: list内嵌dic
        """
        query_set = self.liked_public_elective_courses.all().order_by('course_time__weekday').distinct()
        data = self.get_public_elective_courses_details_and_relations(query_set)
        return data

    # @student_data_cache
    def get_public_elective_selection_failed_courses_from_database(self):
        """
        从数据库内获取选课失败列表
        :return: list内嵌dic
        """
        query_set = self.selection_fail_courses.all()
        data = self.get_public_elective_courses_details_and_relations(query_set)
        return data

    def get_circulate_selection_courses(self):
        """
        淘课啦需要每天自动提交的循环选课列表
        如果不在选课季内了就清空循环选课列表
        :return: list内嵌dic
        """
        if is_public_elective_course_season:
            query_set = self.circulate_selection_courses.all()
            data = self.get_public_elective_courses_details_and_relations(query_set)
            return data, True
        else:
            if self.circulate_selection_courses:
                for course in self.circulate_selection_courses.all():
                    self.circulate_selection_courses.remove(course)
            # not in public elective season
            return [], False

    def get_public_elective_courses_details_and_relations(self, query_set):
        """
        relations暂时仅包含'has_selected''has_collected''has_circulated'
        :param query_set: QuerySet[PublicElectiveCourse]
        :return:
        """
        all_detail = []
        query_set = PublicElectiveCourse.get_all_detail_prefetch(query_set)
        liked_courses = self.liked_public_elective_courses.all()
        circulate_selection_courses = self.circulate_selection_courses.all()
        selected_courses = PublicElectiveCourse.objects.filter(student_courses__student=self)
        for course in query_set:
            detail = course.get_course_detail()
            if course in liked_courses:
                detail['has_collected'] = True
            else:
                detail['has_collected'] = False

            if course in circulate_selection_courses:
                detail['has_circulated'] = True
            else:
                detail['has_circulated'] = False

            if course in selected_courses:
                detail['has_selected'] = True
            else:
                detail['has_selected'] = False
            all_detail.append(detail)
        return all_detail

    def get_relations_with_course(self, course):
        """
        获取学生与某个课程的关系
        是否收藏，是否加入循环选课列表，是否可以评价
        :param course: PublicElectiveCourse instance
        :return:
        """
        result = {
            'has_selected': False,
            'has_collected': False,
            'has_circulated': False,
            'can_comment': False,
            'has_commented': False,
        }
        # 如果只是要看是否存在的，那么exists()函数正好，不需要取到全部结果然后用if判断
        if self.liked_public_elective_courses.filter(id=course.id).exists():
            result['has_collected'] = True
        if self.circulate_selection_courses.filter(id=course.id).exists():
            result['has_circulated'] = True
        # 注释掉的这句话会造成一次多余的course_set的查询，但是需要的course_set的id可以直接在已经取到的course表中找到
        # search_student_course = self.publicelectivestudentcourse_set.filter(course_set=course.course_set)
        search_student_course = self.publicelectivestudentcourse_set.filter(course_set=course.course_set_id)
        if search_student_course:
            result['has_selected'] = True
            for student_course in search_student_course:
                score = student_course.score
                result['score'] = score
                if not student_course.has_commented and score != -1.0:
                    result['can_comment'] = True
                elif student_course.has_commented:
                    result['has_commented'] = True
        return result

    @must_be_in_the_public_elective_course_season
    def add_public_elective_course_and_submit(self, course_number_to_select):
        """
        增加一门选课并提交至教务系统
        :param course_number_to_select: 要选的课的课头号
        :return: string: message, bool: success
        """
        course_number_list, success = self.get_selecting_courses_number_list()
        if success:
            # 添加到临时选课列表并传入API
            # 临时选课列表不会存入数据库
            if str(course_number_to_select) not in course_number_list:
                course_number_list.append(course_number_to_select)
            if len(course_number_list) <= settings.RESTRICTIONS['MAX_PUBLIC_ELECTIVE_COURSE_TO_SELECT']:
                self.temp_selection_course_number_list = course_number_list
                result, success = API.select_public_selective_course(self)
                if success:
                    self.selection_list_submit_time = datetime.date.today()
                    self.save()
                    return 0, True
                else:
                    logger.error(u'API Error %s, failed: student %s submit public selective course list' %
                                 (result, self.number))
                    return result, False
            else:
                # maximum public elective course amount reached
                return 2, False
        else:
            error_code = course_number_list
            return error_code, False

    @must_be_in_the_public_elective_course_season
    def submit_all_circulate_selection_course(self):
        """
        提交所有循环选课的课程
        提交前刷新前一天的选课结果
        此方法仅供清晨celery自动发请求，因为会清除选课列表中的其他课程，所有严禁其他时间使用
        :return:
        """
        # 这里是为了先保存选课结果
        error_number, success = self.get_selecting_courses()
        if success:
            selection_course_list = []
            for course in self.circulate_selection_courses.all():
                selection_course_list.append(course.number)
            self.temp_selection_course_number_list = selection_course_list
            result, success = API.select_public_selective_course(self)
            if success:
                self.selection_list_submit_time = datetime.date.today()
                self.save()
                return 0, True
            else:
                logger.error(u'API Error %s, failed: student %s submit public selective course list' %
                             (result, self.number))
                return result, False
        else:
            return error_number, False

    @must_be_in_the_public_elective_course_season
    def delete_selection_course(self, number):
        selecting_courses_list, success = self.get_selecting_courses_number_list()
        if success:
            try:
                selecting_courses_list.remove(int(number))
            except ValueError:
                return 4, False
            if len(selecting_courses_list) >= 1:
                self.temp_selection_course_number_list = selecting_courses_list
                result, success = API.select_public_selective_course(self)
                if success:
                    return 0, True
                else:
                    error_code = result
                    return error_code, False
            else:
                # 由于教务系统的bug，选课表上的最后一门课是无法删除的
                # 不要问我为什么。有本事你去debug教务系统啊！！
                return 555, False
        else:
            error_code = selecting_courses_list
            return error_code, False

    @refresh_public_elective_selection_result
    def get_selecting_courses(self, failed_courses, success_courses, selecting_courses):
        """
        由于装饰器的存在，其实际参数表为(student, force=None)
        """
        return selecting_courses, True

    @refresh_public_elective_selection_result
    def get_selecting_courses_number_list(self, failed_courses, success_courses, selecting_courses):
        """
        由于装饰器的存在，其实际参数表为(student, force=None)
        """
        return list(selecting_courses.values_list('number', flat=True)), True

    @refresh_public_elective_selection_result
    def check_if_need_send_sms(self, failed_courses, success_courses, selecting_courses):
        # 没有待处理的课程但是有选课成功的课程，说明今天没有提交选课，会导致多次重复发消息
        if not selecting_courses and success_courses:
            self.need_send_sms = False
            self.save()
        return 0, True

    @refresh_public_elective_selection_result
    def send_message_of_public_elective_selection_result(self, failed_courses, success_courses, selecting_courses):
        """
        由于装饰器的存在，其实际参数表为(student, force=None)
        """
        if self.sms_service and self.contact_number:
            for course in success_courses:
                AliyunAPI.send_message(student=self, course=course)
        return 0, True

    @must_be_in_the_public_elective_course_season
    @refresh_public_elective_selection_result
    def get_selecting_courses_detail_with_student_relations(self, failed_courses, success_courses, selecting_courses):
        detail_list = self.get_public_elective_courses_details_and_relations(selecting_courses)
        return detail_list, True

    @must_be_in_the_public_elective_course_season
    def add_circulate_selection_courses(self, course_number_to_select):
        """
        添加循环选课课程，并向教务系统检查是否在现有的选课列表中
        :param course_number_to_select: 要添加课程的课头号
        :return: string:message, bool:success
        """
        # 直接添加循环选课，因为正常来说在选课列表中的才能添加循环选课
        try:
            course = PublicElectiveCourse.objects.get(number=course_number_to_select)
        except PublicElectiveCourse.DoesNotExist:
            # course do not exist
            return 4, False
        self.circulate_selection_courses.add(course)
        return 'success', True

    @must_be_in_the_public_elective_course_season
    def delete_circulate_selection_courses(self, course_number_to_delete):
        """
        添加循环选课课程，并向教务系统检查是否在现有的选课列表中
        :param course_number_to_delete: 要删除课程的课头号
        :return: string:message, bool:success
        """
        # 直接添加循环选课，因为正常来说在选课列表中的才能添加循环选课
        try:
            course = PublicElectiveCourse.objects.get(number=course_number_to_delete)
        except PublicElectiveCourse.DoesNotExist:
            # course do not exist
            return 4, False
        if course in self.circulate_selection_courses.all():
            self.circulate_selection_courses.remove(course)
            return 'success', True
        else:
            return 4, True

    def add_liked_courses(self, course_number):
        """
        添加收藏课程
        :param course_number: 要添加课程的课头号
        :return: string:message, bool:success
        """
        # 失效缓存
        cache.set('%s_get_liked_courses' % self.number, None)
        try:
            course = PublicElectiveCourse.objects.get(number=course_number)
        except PublicElectiveCourse.DoesNotExist:
            logger.error(u'student %s visiting course %s that is not exist' % (self, course_number))
            return 4, False
        self.liked_public_elective_courses.add(course)
        logger.info(u'student %s add liked course %s' % (self, course_number))
        return 'success', True

    def delete_liked_courses(self, course_number):
        """
        删除收藏课程
        :param course_number: 所删除的课程课头号
        :return:  int: error_code, bool:success
        """
        # 失效缓存
        cache.set('%s_get_liked_courses' % self.number, None)
        if self.liked_public_elective_courses.filter(number=course_number):
            target = self.liked_public_elective_courses.get(number=course_number)
            self.liked_public_elective_courses.remove(target)
            logger.info(u'student %s delete liked course %s' % (self, course_number))
            return 0, True
        else:
            logger.error(u'student %s delete liked course %s which not in the liked list' % (self, course_number))
            # 此课程不存在
            return 4, False

    def rebind_token(self):
        """
        用密码去重新获取token和rf_token
        :return: string:message, bool:success
        """
        result, success = API.get_token(self)
        if success:
            self.token = result['token']
            self.rf_token = result['refresh_token']
            # todo: expires in
            self.save()
            logger.info(u'student %s refresh token' % self.number)
            return 'success', True
        else:
            logger.error(u'API ERROR %s, failed: student %s rebind token' % (result, self.number))
            return result, False

    def refresh_token(self):
        """
        用已经过期的token和rf_token去快速更新
        :return: string:message, bool:success
        """
        result, success = API.refresh_token(self)
        if success:
            self.token = result['token']
            self.rf_token = result['refresh_token']
            # todo: expires in
            self.save()
            logger.info(u'student %s refresh token' % self.number)
            return 'success', True
        else:
            logger.error(u'API ERROR%s, failed: student %s refresh token' % (result, self.number))
            return result, False

    # def check_token_status(self):
    #     """
    #     检查token过期时间，如果token已经过期则自动更新
    #     暂时还没有用处
    #     :return: int 过期时间单位为秒, -1代表出现了某些错误
    #     """
    #     result, success = API.check_token_status(self)
    #     if success and result:
    #         if result['expires_in'] == -1:
    #             refresh_success = self.refresh_token()
    #             if refresh_success:
    #                 # todo: 这里return暂时是写死的，启用的时候考虑重写
    #                 return 7200
    #             else:
    #                 return -1
    #         return result['expires_in']
    #     else:  # 以为token可能不对，所以success为True但是result依旧可能为空
    #         if result:
    #             logger.info(result)
    #         else:
    #             logger.info('student %s check token status failed' % self.number)
    #         return -1

    def update_student_course(self):
        """
        更新选课，删除已经选上的循环选课，撤课，课程分数
        :return:
        """
        data, success = API.get_course(self)
        if success:
            course_number_list = []
            for course_data in data:
                if course_data['identifier'].startswith('ck'):
                    # 不及格补考会再教务系统中出现一门重考的课程
                    # 重考的课头号是ck开头的会造成存数据爆炸
                    # 而且重考的课程没什么卵用来着
                    # 所以选择忽略
                    continue
                course_number_list.append(course_data['identifier'])
                # 添加新课和更新
                course = CourseBase.get_or_create_course(course_data)
                student_course = StudentCourseBase.get_or_create_student_course(student=self, course=course,
                                                                                learning_type=
                                                                                course_data['learning_type'])
                if not student_course.score == course_data['grade']:
                    student_course.score = course_data['grade']
                    student_course.save()
            # 处理撤课
            for student_course in self.publicelectivestudentcourse_set.all()\
                    .select_related('course').only('course__number', 'student'):
                if str(student_course.course.number) not in course_number_list:
                    student_course.delete()
            for student_course in self.specializedstudentcourse_set.all()\
                    .select_related('course').only('course__number', 'student'):
                if str(student_course.course.number) not in course_number_list:
                    student_course.delete()
            for student_course in self.publiccompulsorystudentcourse_set.all()\
                    .select_related('course').only('course__number', 'student'):
                if str(student_course.course.number) not in course_number_list:
                    student_course.delete()
            self.course_table_update_time = datetime.date.today()
            self.save()
            return True
        return False

    @check_if_courses_need_update
    # @student_data_cache
    def get_all_course_this_semester_for_curriculum(self):
        """
        获取课表来更新数据库
        更新新课或是更新出分
        然后返回课表
        :return:
        """
        data = []
        current_week = get_current_week()
        semester_id = str(settings.SEMESTER_DATE['YEAR']) + str(settings.SEMESTER_DATE['SEMESTER'])
        # 公选课不filter is_open 的原因是有些看似专业课然而是公选课，F但是不能标记is_open是因为不能在课程筛选中出现
        query_set = PublicElectiveCourse.objects.filter(number__startswith=semester_id, student_courses__student=self) \
            .distinct()
        detail_list = PublicElectiveCourse.get_all_detail(query_set)
        for detail in detail_list:
            pop_list = []
            course_time_index = 0
            # 记录没用的course_time的index
            for course_time in detail['time']:
                # 课程还没开始或者结束了
                if not (course_time['week_from'] <= current_week <= course_time['week_to']):
                    pop_list.append(course_time_index)
                # 由于单双周的安排这周没这个时段的课
                elif (current_week - course_time['week_from']) % course_time['repeat']:
                    pop_list.append(course_time_index)
                course_time_index += 1
            # 把没用的course_time pop出来
            # 因为前面的item pop出来会影响后面的index, 所以倒着pop
            for i in reversed(pop_list):
                detail['time'].pop(i)
            detail['is_public_elective'] = True
            # 经过筛选还有课程有效时间的加入课表结果
            if detail['time']:
                data.append(detail)
        query_set = SpecializedCourse.objects.filter(is_open=True, student_courses__student=self).distinct()
        detail_list = SpecializedCourse.get_all_detail(query_set)
        for detail in detail_list:
            pop_list = []
            course_time_index = 0
            # 记录没用的course_time的index
            for course_time in detail['time']:
                # 课程还没开始或者结束了
                if not (course_time['week_from'] <= current_week <= course_time['week_to']):
                    pop_list.append(course_time_index)
                # 由于单双周的安排这周没这个时段的课
                elif (current_week - course_time['week_from']) % course_time['repeat']:
                    pop_list.append(course_time_index)
                course_time_index += 1
            # 把没用的course_time pop出来
            # 因为前面的item pop出来会影响后面的index, 所以倒着pop
            for i in reversed(pop_list):
                detail['time'].pop(i)
            detail['is_public_elective'] = False
            # 经过筛选还有课程有效时间的加入课表结果
            if detail['time']:
                data.append(detail)
        query_set = PublicCompulsoryCourse.objects.filter(is_open=True, student_courses__student=self).distinct()
        detail_list = PublicCompulsoryCourse.get_all_detail(query_set)
        for detail in detail_list:
            pop_list = []
            course_time_index = 0
            # 记录没用的course_time的index
            for course_time in detail['time']:
                # 课程还没开始或者结束了
                if not (course_time['week_from'] <= current_week <= course_time['week_to']):
                    pop_list.append(course_time_index)
                # 由于单双周的安排这周没这个时段的课
                elif (current_week - course_time['week_from']) % course_time['repeat']:
                    pop_list.append(course_time_index)
                course_time_index += 1
            # 把没用的course_time pop出来
            # 因为前面的item pop出来会影响后面的index, 所以倒着pop
            for i in reversed(pop_list):
                detail['time'].pop(i)
            detail['is_public_elective'] = False
            # 经过筛选还有课程有效时间的加入课表结果
            if detail['time']:
                data.append(detail)
        return data, True

    def get_gpa(self, force=None):
        """
        获取gpa
        :param force: 是否强制更新
        :return: float: data/ string: message, bool: success
        """
        if self.gpa and not force:
            return self.gpa, True
        else:
            result, success = API.get_gpa(self)
            if success:
                logger.info(u'student %s get gpa success' % self.number)
                self.gpa = result['gpa']
                self.save()
                return result['gpa'], True
            else:
                logger.error(u'API ERROR %s, failed: student %s get gpa success' % (result, self.number))
                return None, False

    # @student_data_cache
    def get_selective_course_credit_count(self):
        """
        :return: dic 各个领域已选公选课学分总数和对应的课程详情
        示例返回：
        [
            {
                'field': '数学推理类',
                'credit': 2,
                'courses': [
                    {
                        'name': '课程1',
                        'credit': 1,
                    },
                    {
                        'name': '课程2',
                        'credit': 1,
                    }
                ],
            },
            {
                'field': '自然与工程类',
                'credit': 0,
                'courses': [],
            }
        ]
        """
        result_dic = {
            u'未知': {
                'field': u'未知',
                'credit': 0,
                'courses': [],
            }
        }
        for field in PublicElectiveCourse.COURSE_FIELD_CHOICE:
            field_name = field[0]
            if field_name in [u'未知', u'无领域', u'无']:
                pass
            else:
                result_dic[field_name] = {
                    'field': field_name,
                    'credit': 0,
                    'courses': [],
                }
        for student_course in self.publicelectivestudentcourse_set.filter(learning_type=u'普通')\
                .select_related('course') \
                .only('student', 'course__name', 'course__number', 'course__credit', 'course__field'):
            course_field_name = student_course.course.field
            if course_field_name in [u'未知', u'无领域', u'无', None]:
                result_dic[u'未知']['credit'] += student_course.course.credit
                result_dic[u'未知']['courses'].append(student_course.course.get_course_abstract())
            else:
                result_dic[course_field_name]['credit'] += student_course.course.credit
                result_dic[course_field_name]['courses'].append(student_course.course.get_course_abstract())
        result_list = []
        for field in PublicElectiveCourse.COURSE_FIELD_CHOICE:
            field_name = field[0]
            if field_name not in [u'未知', u'无领域', u'无']:
                result_list.append(result_dic[field_name])
        result_list.append(result_dic[u'未知'])
        return result_list

    # @student_data_cache
    def get_all_uncomment_course(self):
        """
        :return: list 返回所有没有评价过的课程的课头号
        """
        # 解决重修课要求重复评价
        query_filter = models.Q(student_courses__student=self) & \
            models.Q(student_courses__has_commented=False) & \
            models.Q(student_courses__score__gt=0)
        query_set = PublicElectiveCourse.objects.filter(query_filter).distinct()
        all_uncomment_course = PublicElectiveCourse.get_all_detail(query_set)
        return all_uncomment_course

    # 暂时不会用，但是会涉及到外表优化的问题，如果有一天启用请优化后投入生产
    # def get_all_my_comment(self):
    #     """
    #     :return: 我所有的评论的详情（附带评论的id）
    #     """
    #     data = []
    #     for comment in self.comment_set.all():
    #         data.append(comment.get_comment_detail())
    #     return data

    # @student_data_cache
    def get_all_specialized_course(self):
        """
        :return: list 所有的专业课的详情
        """
        # 通过判断学生学号前缀推断入学年龄，再通过学生课程的年份信息是该学生大几的课
        delta_year_grade_map = {
            0: u'大一',
            1: u'大二',
            2: u'大三',
            3: u'大四',
            4: u'大五',
            5: u'大六',
            6: u'大七',
            7: u'大八',
            8: u'大九',
            # 如果不属于这些就是其他年级
        }
        student_year = int(str(self.number)[0:4])
        all_specialized_course = {}
        query_set = self.specializedstudentcourse_set.all()
        detail_list = PublicCompulsoryStudentCourse.get_all_course_detail(query_set)
        for detail in detail_list:
            course_year = int(str(detail['number'])[0:4])
            delta_year = course_year - student_year
            if delta_year in delta_year_grade_map:
                grade = delta_year_grade_map[delta_year]
            else:
                grade = u'其他年级'
            if grade in all_specialized_course:
                all_specialized_course[grade].append(detail)
            else:
                all_specialized_course[grade] = [detail]
        return all_specialized_course

    # @student_data_cache
    def get_all_recommended_specialized_course(self):
        """
        :return: list 所有的已经推荐的专业课的详情
        """
        query_set = self.specializedstudentcourse_set.filter(has_recommended=True)
        all_recommended_specialized_course = SpecializedStudentCourse.get_all_course_detail(query_set)
        return all_recommended_specialized_course

    def recommend_specialized_course(self, number):
        """
        1.检查是否达到推荐课程的上限
        2.对应的 student_course 的 has_recommended 置 True
        3.对应的 same_course_set 的 recommend_count 加一
        :param number: 推荐的课程的课头号
        :return: string: message, success:bool
        """
        # 失效相关的缓存
        cache.set('%s_get_all_recommended_specialized_course' % self.number, None)
        # 使用queryset.count()而不是len(self.get_all_recommended_specialized_course())是出于性能的考虑
        has_recommended_course_count = self.specializedstudentcourse_set.filter(has_recommended=True).count()
        if has_recommended_course_count < settings.RESTRICTIONS['MAX_RECOMMEND_SPECIALIZED_COURSE']:
            try:
                student_course = self.specializedstudentcourse_set.get(course__number=number)
            except SpecializedStudentCourse.DoesNotExist:
                logger.error(u'student %s recommend specialized course %s does not exist' % (self.number, number))
                return 4, False
            student_course.has_recommended = True
            student_course.recommend_time = datetime.datetime.today()
            student_course.save()
            if student_course.course_set.recommend_count:
                student_course.course_set.recommend_count += 1
            else:
                student_course.course_set.recommend_count = 1
            student_course.course_set.save()
            logger.info(u'student %s recommend specialized course %s' % (self.number, number))
            return 0, True
        else:
            # maximum specialized course recommend amount reached
            return 3, False

    def delete_recommended_specialized_course(self, number):
        """
        1.对应的 student_course 的 has_recommended 置 False
        2.对应的 same_course_set 的 recommend_count 减一
        :param number: 推荐的课程的课头号
        :return: string: message, success:bool
        """
        # 失效相关的缓存
        cache.set('%s_get_all_recommended_specialized_course' % self.number, None)
        try:
            student_course = self.specializedstudentcourse_set.get(course__number=number)
        except SpecializedStudentCourse.DoesNotExist:
            logger.error(u'student %s delete recommend specialized course %s does not exist' % (self.number, number))
            # course do not exist
            return 4, False
        if student_course.has_recommended:
            student_course.has_recommended = False
            student_course.recommend_time = None
            student_course.save()
        else:
            logger.error(u'student %s delete recommend specialized course %s does not actually recommended' %
                         (self.number, number))
            # can't delete things do not exist
            return 5, True
        student_course.course_set.recommend_count -= 1
        student_course.course_set.save()
        logger.info(u'student %s delete recommend specialized course %s' % (self.number, student_course.course.number))
        return 0, True

    def get_all_pop_up_notice(self):
        """
        根据弹窗是否重复弹出，今天是否查看过，和是否设置不再提醒筛选要显示的弹窗
        :return: list
        """
        data = []
        all_notice = PopUpNotice.objects.all()
        all_notice_relations = StudentNoticeRelations.objects.filter(student=self).select_related('notice')
        if len(all_notice) == len(all_notice_relations):
            for notice_relation in all_notice_relations:
                # 检查是否需要弹出
                if notice_relation.if_needs_to_pop():
                    data.append(notice_relation.notice.get_pop_up_detail())
        else:
            # 数量不符，按需新建中间表
            for notice in all_notice:
                try:
                    notice_relation = StudentNoticeRelations.objects.get(notice=notice, student=self)
                except StudentNoticeRelations.DoesNotExist:
                    notice_relation = StudentNoticeRelations(notice=notice, student=self)
                    notice_relation.save()
                if notice_relation.if_needs_to_pop():
                    data.append(notice_relation.notice.get_pop_up_detail())
        return data

    def mark_notice_as_read(self, notice_id, do_not_remind):
        """
        把重复弹出的弹框标记为不再提示
        :return:
        """
        # todo: 没看
        try:
            notice = PopUpNotice.objects.get(id=notice_id)
        except PopUpNotice.DoesNotExist:
            return 12, False
        if notice.studentnoticerelations_set.filter(student=self):
            student_notice_relations = notice.studentnoticerelations_set.get(student=self)
        else:
            student_notice_relations = StudentNoticeRelations(notice=notice, student=self)
        student_notice_relations.read_date = datetime.date.today()
        if do_not_remind:
            student_notice_relations.do_not_remind = True
        student_notice_relations.save()
        return 0, True

    def set_contact_number(self, contact_number):
        if contact_number:
            if str(contact_number).isdigit():
                if len(contact_number) == 11:
                    self.contact_number = int(contact_number)
                    self.sms_service = True
                    self.save()
                    return True
        return False

    def enable_sms_service(self):
        self.sms_service = True
        self.save()

    def disable_sms_service(self):
        self.sms_service = False
        self.save()

    @staticmethod
    def student_login(student_number, student_pwd):
        """
        如果成功，返回的是 user，True
        如果失败，返回的是error_code, False
        :param student_number:
        :param student_pwd:
        :return:
        """
        student_query_set = Student.objects.filter(number=student_number)
        if student_query_set:
            user = authenticate(username=student_number, password=student_pwd)
            student = student_query_set[0]
            # 防止因为EduPwdError清空密码的用户再次成功登录
            if user and student.password:
                return user, True
            else:
                # 此处为用户输入的密码和数据库内储存的密码不一致
                # 保存旧数据以便回滚
                old_pwd = student.password
                # 将数据库里密码改为新密码，传入掌武api验证
                # 如果密码错误，直接raise出来EduPwdError被上层view函数的装饰器捕获
                student.password = student_pwd
                student.save()
                try:
                    result, success = API.get_token(student=student)
                except EduPwdError:
                    # 这里再捕获一次是为了回滚原来的密码
                    student.password = old_pwd
                    student.save()
                    # 再次raise错误给view函数捕获
                    raise EduPwdError(student)
                if success:
                    student.token = result['token']
                    student.rf_token = result['refresh_token']
                    student.password = student_pwd
                    student.user.set_password(student_pwd)
                    student.user.save()
                    student.save()
                    user = authenticate(username=student_number, password=student_pwd)
                    return user, True
                else:
                    return result, False
        else:
            # 此处为新用户第一次登陆
            result, success = Student.new_user(student_number, student_pwd)
            if success:
                user = authenticate(username=student_number, password=student_pwd)
                return user, True
            else:
                error_code = result
                return error_code, False

    @staticmethod
    def new_user(student_number, student_pwd):
        """
        新建新用户
        :param student_number:
        :param student_pwd:
        :return:
            成功: student, True
            失败: error_code, False
        """
        student_query_set = Student.objects.filter(number=student_number)
        if student_query_set:
            return student_query_set[0], True
        else:
            if User.objects.filter(username=student_number):
                new_user = User.objects.get(username=student_number)
            else:
                new_user = User.objects.create_user(username=student_number, password=student_pwd)
            new_student = Student(number=student_number, password=student_pwd, user=new_user)
            new_student.save()
            result, success = API.get_token(new_student)
            if success:
                new_student.token = result['token']
                new_student.rf_token = result['refresh_token']
                new_student.save()
            else:
                new_student.delete()
                new_user.delete()
                return result, False
            result, success = API.get_information(new_student)
            if success:
                new_student.name = result['name']
                try:
                    academy = Academy.objects.get(name=result['college'])
                except Academy.DoesNotExist:
                    academy = Academy(name=result['college'])
                    academy.save()
                new_student.academy = academy
                new_student.save()
                result, success = API.get_gender(new_student)
                if success:
                    new_student.gender = result['gender']
                    new_student.save()
                    return new_student, True
                else:
                    new_student.delete()
                    new_user.delete()
                    return result, False
            else:
                new_student.delete()
                new_user.delete()
                return result, False


class StudentCourseBase(models.Model):
    LEARNING_TYPE_CHOICE = (
        (u'普通', u'普通'),
        (u'重修', u'重修'),
        (u'第二次重修', u'第二次重修'),
        (u'及格重修', u'及格重修'),
        (u'辅修', u'辅修'),
    )

    learning_type = models.CharField(verbose_name=u'学习类型', default=u'普通', choices=LEARNING_TYPE_CHOICE,
                                     max_length=20, blank=True, null=True)
    score = models.FloatField(verbose_name=u'课程得分', default=-1.0, blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name = u'学生课程基类'
        verbose_name_plural = u'学生课程基类'

    @staticmethod
    def get_or_create_student_course(student, course, learning_type):
        if isinstance(course, PublicElectiveCourse):
            try:
                student_course = PublicElectiveStudentCourse.objects.get(student=student, course=course,
                                                                         learning_type=learning_type)
            except PublicElectiveStudentCourse.DoesNotExist:
                student_course = PublicElectiveStudentCourse(student=student, course=course,
                                                             course_set=course.course_set, learning_type=learning_type)
                student_course.save()
        elif isinstance(course, SpecializedCourse):
            try:
                student_course = SpecializedStudentCourse.objects.get(student=student, course=course,
                                                                      learning_type=learning_type)
            except SpecializedStudentCourse.DoesNotExist:
                student_course = SpecializedStudentCourse(student=student, course=course,
                                                          course_set=course.course_set, learning_type=learning_type)
                student_course.save()
        elif isinstance(course, PublicCompulsoryCourse):
            try:
                student_course = PublicCompulsoryStudentCourse.objects.get(student=student, course=course,
                                                                           learning_type=learning_type)
            except PublicCompulsoryStudentCourse.DoesNotExist:
                student_course = PublicCompulsoryStudentCourse(student=student, course=course,
                                                               course_set=course.course_set,
                                                               learning_type=learning_type)
                student_course.save()
        else:
            return None
        return student_course


class PublicElectiveStudentCourse(StudentCourseBase):
    student = models.ForeignKey(verbose_name=u'对应学生', to=Student, blank=True, null=True)
    course = models.ForeignKey(verbose_name=u'对应课程', to=PublicElectiveCourse, related_name='student_courses',
                               blank=True, null=True)
    course_set = models.ForeignKey(verbose_name=u'对应课程集', to=PublicElectiveCourseSet, blank=True, null=True)
    has_commented = models.BooleanField(verbose_name=u'是否评论过', default=False)

    # 取出该用户该课程的评价 可以通过 (PublicElectiveStudentCourse_instance).comment 获得，因为是OneToOneField

    class Meta:
        verbose_name = u'学生公选课程'
        verbose_name_plural = u'学生公选课程'

    def __unicode__(self):
        return u'%s的%s公选课程' % (self.student, self.course)

    @staticmethod
    def get_all_course_detail(query_set):
        """
        :param query_set: QuerySet[PublicElectiveStudentCourse]
        :return:
        """
        course_id_list = []
        for student_course in query_set:
            course_id_list.append(student_course.course_id)
        course_query_set = PublicElectiveCourse.objects.filter(id__in=course_id_list)
        result = PublicElectiveCourse.get_all_detail(course_query_set)
        return result


class SpecializedStudentCourse(StudentCourseBase):
    student = models.ForeignKey(verbose_name=u'对应学生', to=Student, blank=True, null=True)
    course = models.ForeignKey(verbose_name=u'对应课程', to=SpecializedCourse, related_name='student_courses',
                               blank=True, null=True)
    course_set = models.ForeignKey(verbose_name=u'对应课程集', to=SpecializedCourseSet, blank=True, null=True)
    has_recommended = models.BooleanField(verbose_name=u'是否推荐过', default=False)
    recommend_time = models.DateTimeField(verbose_name=u'推荐的时间',
                                          auto_now=False, auto_now_add=False, blank=True, null=True)

    class Meta:
        verbose_name = u'学生专业课程'
        verbose_name_plural = u'学生专业课程'

    def __unicode__(self):
        return u'%s的%s专业课程' % (self.student, self.course)

    @staticmethod
    def get_all_course_detail(query_set):
        """
        :param query_set: QuerySet[SpecializedStudentCourse]
        :return:
        """
        course_id_list = []
        for student_course in query_set:
            course_id_list.append(student_course.course_id)
        course_query_set = SpecializedCourse.objects.filter(id__in=course_id_list)
        result = SpecializedCourse.get_all_detail(course_query_set)
        return result


class PublicCompulsoryStudentCourse(StudentCourseBase):
    student = models.ForeignKey(verbose_name=u'对应学生', to=Student, blank=True, null=True)
    course = models.ForeignKey(verbose_name=u'对应课程', to=PublicCompulsoryCourse, related_name='student_courses',
                               blank=True, null=True)
    course_set = models.ForeignKey(verbose_name=u'对应课程集', to=PublicCompulsoryCourseSet, blank=True, null=True)

    class Meta:
        verbose_name = u'学生公必课程'
        verbose_name_plural = u'学生公必课程'

    def __unicode__(self):
        return u'%s的%s公必课程' % (self.student, self.course)

    @staticmethod
    def get_all_course_detail(query_set):
        """
        :param query_set: QuerySet[PublicCompulsoryStudentCourse]
        :return:
        """
        course_id_list = []
        for student_course in query_set:
            course_id_list.append(student_course.course_id)
        course_query_set = PublicCompulsoryCourse.objects.filter(id__in=course_id_list)
        result = PublicCompulsoryCourse.get_all_detail(course_query_set)
        return result


class Comment(models.Model):
    GIVING_SCORE_VALUATION_CHOICE = (
        ('good', 'good'),
        ('medium', 'medium'),
        ('bad', 'bad'),
    )

    course_set = models.ForeignKey(verbose_name=u'评价对应课程集', to=PublicElectiveCourseSet, blank=True, null=True)
    course = models.ForeignKey(verbose_name=u'评价对应课程', to=PublicElectiveCourse, blank=True, null=True)
    student = models.ForeignKey(verbose_name=u'评价的学生', to=Student, default=None)
    student_course = models.OneToOneField(verbose_name=u'对应的用户课程中间表', to=PublicElectiveStudentCourse,
                                          blank=True, null=True)
    content = models.TextField(verbose_name=u'评价的内容')
    is_anonymous = models.BooleanField(verbose_name=u'是否匿名并隐藏头像', default=False)
    publish_time = models.DateTimeField(verbose_name=u'评论发布时间', auto_now_add=True)
    check_attendance_valuation = models.CharField(verbose_name=u'考勤力度评分', max_length=10,
                                                  choices=GIVING_SCORE_VALUATION_CHOICE, blank=True, null=True)
    teaching_quality_valuation = models.CharField(verbose_name=u'授课质量评分', max_length=10,
                                                  choices=GIVING_SCORE_VALUATION_CHOICE, blank=True, null=True)
    giving_score_valuation = models.CharField(verbose_name=u'给分情况评分', max_length=10,
                                              choices=GIVING_SCORE_VALUATION_CHOICE, blank=True, null=True)

    class Meta:
        verbose_name = u'课程评价'
        verbose_name_plural = u'课程评价'
        ordering = ['-publish_time']

    def __unicode__(self):
        return u'%s对%s的评价' % (self.student, self.course)

    def get_comment_detail(self):
        """
        :return: dic 评论详细信息
        """
        if self.publish_time:
            time_str = self.publish_time.strftime("%Y年%m月%d日")
        else:
            time_str = u'未知发布时间'
        if self.is_anonymous:
            data = {
                'student': {
                    'name': u'匿名',
                    'avatar': '/static/img/hand_img/default/%s.png' % random.randint(1, 40),
                },
                'time': time_str,
                'content': self.content,
                'score': {
                    'attendance': self.check_attendance_valuation,
                    'quality': self.teaching_quality_valuation,
                    'score': self.giving_score_valuation,
                },
            }
        else:
            data = {
                'student': {
                    'name': self.student.name,
                    'avatar': self.student.get_avatar(),
                },
                'time': time_str,
                'content': self.content,
                'score': {
                    'attendance': self.check_attendance_valuation,
                    'quality': self.teaching_quality_valuation,
                    'score': self.giving_score_valuation,
                },
            }
        return data

    def save(self, *args, **kwargs):
        """
        构造的时候需要填的字段：course, student, content, is_anonymous
        check_attendance_valuation, teaching_quality_valuation, giving_score_valuation

        在储存的同时
        1.建立course_set的foreignkey和student_course的OneToOneField
        2.将相应的StudentCourse表中的has_commented置为True
        3.将评分同步至同课程集下
        """
        course_set = self.course.course_set
        self.course_set = course_set
        student_course = PublicElectiveStudentCourse.objects.get(student=self.student, course=self.course)
        self.student_course = student_course
        super(Comment, self).save(*args, **kwargs)

        student_course.has_commented = True
        student_course.save()

        exec ('course_set.check_attendance_%s += 1' % self.check_attendance_valuation)
        exec ('course_set.teaching_quality_%s += 1' % self.teaching_quality_valuation)
        course_set.save()

    def delete(self, using=None):
        """
        在删除的同时
        1.删除student_course的OneToOneField，has_commented置为False
        2.课程集下的评分回滚
        :param using:
        """
        student_course = self.student_course
        student_course.has_commented = False
        student_course.save()

        course_set = student_course.course_set
        exec ('course_set.check_attendance_%s -= 1' % self.check_attendance_valuation)
        exec ('course_set.teaching_quality_%s -= 1' % self.teaching_quality_valuation)
        course_set.save()

        super(Comment, self).delete()
