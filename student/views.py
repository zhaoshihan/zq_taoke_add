# -*- coding: utf-8 -*-
"""
view函数代码说明
为了保证model层封装的完整性，保证view函数的精简和可读性，在view函数内尽量不要直接调用表的查询
如果student中没有对应的方法，建议封装进student方法后调用

通常的套路为：
student = request.user.student
接下来调用student封装好的方法
最后json标准返回

标准正确返回
{
    'status': {
        'code': 0,
        'message': 'success',
    }
    '***': -*- data_body -*-,
}

标准错误返回
{
    'status': {
        'code': error_code,
        'message': ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code),,
    }
}

其中error_code一般是student的model函数的返回错误代码
其中message，即前端错误提示的文案
使用ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code)取出
!!!凡是用户可能看见的错误提示一律用此方法取出文案
所有不用此方法取出文案，返回固定的错误信息的必须保证为开发错误，生产过程中不能遇见

所有访问加教务系统的view函数必须加上此装饰器
说明edu_pwd_error_handler：
所有有关教务系统的查询有可能涉及到教务系统的密码错误
下层的封装会raise出来EduPwdError并清空数据库中此用户的教务系统密码，
使用此decorator进行装饰，可以捕获EduPwdError并执行登出操作
（下层的封装没有request变量的access，所以登出操作必须在view函数内完成）

所有函数必须配有注释！！！
"""
import json

from functools import wraps

from django.contrib.auth import login as sys_login, logout as sys_logout
from django.http import JsonResponse

from api.APIErrors import EduPwdError
from course.models import PublicElectiveCourseSet, PublicElectiveCourse, SpecializedCourseSet, SpecializedCourse
from student.models import Student, PublicElectiveStudentCourse, SpecializedStudentCourse, Comment
from suggestion.models import ErrorCodeToMessage, PopUpNotice

# Create your views here.


def edu_pwd_error_handler(method):
    """
    捕捉EduPwdError并执行登出操作
    :param method:
    :return:
    """
    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(request, *args, **kwargs):
        try:
            # 如果上层装饰器直接json返回了这里第一个参数其实并不是request，但是并不妨碍功能执行
            return method(request, *args, **kwargs)
        except EduPwdError:
            sys_logout(request)
            data = {
                "status": {
                    "code": -6,
                    "message": ErrorCodeToMessage.cast_error_code_to_message_for_front_end(-6)
                }
            }
            return JsonResponse(data, status=400)

    return wrapper


def customized_login_required(method):
    """
    自动检测登陆状态并执行错误返回
    :param method:
    :return:
    """
    @wraps(method)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated():
            if request.user.is_staff:
                # 如果是超级用户就执行登出然后返回未登录状态
                sys_logout(request)
                status = {'code': 9, 'message': ErrorCodeToMessage.cast_error_code_to_message_for_front_end(9)}
                result = {'status': status}
                return JsonResponse(result, status=401)
            else:
                return method(request, *args, **kwargs)
        else:
            # 用户未登陆
            status = {'code': 9, 'message': ErrorCodeToMessage.cast_error_code_to_message_for_front_end(9)}
            result = {'status': status}
            return JsonResponse(result, status=401)
    return wrapper


@edu_pwd_error_handler
@customized_login_required
def get_stu_info(request):
    """
    获取当前用户信息
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    status['message'] = "成功获取用户信息"
    result.update(student.get_student_detail())
    return JsonResponse(result, status=200)


@edu_pwd_error_handler
def login(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'POST':
        if not request.body:
            request_data = {}
        else:
            request_data = json.loads(request.body)
        number = request_data.get('number')
        password = request_data.get('password')
        if number and password:
            if number.isdigit() and number.__len__() == 13:
                user, success = Student.student_login(int(number), password)
                if success:
                    sys_login(request, user)
                    status['message'] = '登陆成功'
                    result['has_used_sms'] = user.student.sms_service
                    return JsonResponse(result, status=200)
                else:
                    # 错误返回时user返回的是错误码
                    error_code = user
                    status['code'] = error_code
                    status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code)
                    return JsonResponse(result, status=403)
            else:
                # 学号格式错误
                status['code'] = 8
                status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(8)
                return JsonResponse(result, status=403)
        else:
            status['code'] = 400
            status['message'] = 'need more arguments'
            return JsonResponse(result, status=400)
    elif request.method == 'GET':
        status['message'] = '成功获取用户登陆状态'
        if request.user.is_authenticated():
            if request.user.is_staff:
                sys_logout(request)
                result['has_logged_in'] = False
                return JsonResponse(result, status=200)
            else:
                student = request.user.student
                result['has_logged_in'] = True
                result['number'] = str(student.number)
                result['has_used_sms'] = student.sms_service
                return JsonResponse(result, status=200)
        else:
            result['has_logged_in'] = False
            return JsonResponse(result, status=200)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@customized_login_required
def logout(request):
    """
    登出
    :param request:GET
    :return:
    """
    status = {'code': 0, 'message': '登出成功'}
    result = {'status': status}
    sys_logout(request)
    return JsonResponse(result, status=200)


@customized_login_required
def avatar(request):
    """
    处理头像相关
    :param request: GET POST
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'GET':
        result['avatar'] = student.get_avatar()
        status['message'] = '获取用户头像成功'
        return JsonResponse(result, status=200)
    elif request.method == 'PUT':
        result['avatar'] = student.random_sys_avatar()
        status['message'] = '设置随机头像成功'
        return JsonResponse(result, status=200)
    elif request.method == 'POST':
        avatar_file = request.FILES.get('avatar')
        result['avatar'] = student.avatar_file_save(avatar_file)
        status['message'] = '设置自定义头像成功'
        return JsonResponse(result, status=201)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@edu_pwd_error_handler
@customized_login_required
def curriculum(request):
    """
    返回用户课表
    :param request: GET
    :return:    
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    data, success = student.get_all_course_this_semester_for_curriculum()
    if success:
        result['curriculum'] = data
        status['message'] = '获取用户课表成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = data
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(data)
        return JsonResponse(result, status=400)


@customized_login_required
def get_all_public_selected_course(request):
    """
    返回用户已选的公选课列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': '获取用户已选公选课列表成功'}
    result = {'status': status}
    student = request.user.student
    result['selected'] = student.get_selective_course_credit_count()
    return JsonResponse(result, status=200)


@customized_login_required
def get_liked_courses(request):
    """
    返回已收藏公选课列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': '获取用户已收藏公选课列表成功'}
    result = {'status': status}
    student = request.user.student
    result['collected'] = student.get_liked_courses()
    return JsonResponse(result, status=200)


@customized_login_required
def operate_like_course(request, number):
    """
    操作收藏课程
    :param request: POST
    :param number:
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'POST':
        student = request.user.student
        code, is_success = student.add_liked_courses(number)
        if is_success:
            status['message'] = '新增用户已收藏公选课成功'
            return JsonResponse(result, status=201)
        else:
            status['code'] = code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(code)
            return JsonResponse(result, status=400)
    elif request.method == 'DELETE':
        student = request.user.student
        code, is_success = student.delete_liked_courses(number)
        if is_success:
            status['message'] = '删除用户已收藏公选课成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(code)
            return JsonResponse(result, status=400)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@customized_login_required
def get_uncomment_courses(request):
    """
    获取未评价课程列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'GET':
        status['message'] = '获取用户未评价公选课列表成功'
        result['uncomment'] = student.get_all_uncomment_course()
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@customized_login_required
def comment_uncomment_course(request, number):
    """
    评价课程
    :param request:
    :param number:
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'DELETE':
        if not request.body:
            request_data = {}
        else:
            request_data = json.loads(request.body)
        name = request_data.get('name')
        content = request_data.get('comment')
        score_dict = request_data.get('score')
        if number:
            course = PublicElectiveCourse.objects.get(number=number)
        else:
            status['code'] = -2
            status['message'] = '无此课程'
            return JsonResponse(result, status=403)
        if student.name == name or not name:
            Comment.objects.create(course=course, student=student, content=content, is_anonymous=False,
                                   check_attendance_valuation=score_dict['attendance'],
                                   teaching_quality_valuation=score_dict['quality'])
        else:
            Comment.objects.create(course=course, student=student, content=content, is_anonymous=True,
                                   check_attendance_valuation=score_dict['attendance'],
                                   teaching_quality_valuation=score_dict['quality'])
        status['message'] = '评价成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@customized_login_required
def get_selecting_and_failed_courses(request):
    """
    返回用户选课列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'GET':
        data, success = student.get_selecting_courses_detail_with_student_relations()
        if success:
            result['selecting'] = data
            result['failed'] = student.get_public_elective_selection_failed_courses_from_database()
            # 这里取[0]是因为还会返回success字段但是这里不会用
            result['circulating'] = student.get_circulate_selection_courses()[0]
            status['message'] = '获取用户列表成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = data
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(data)
            return JsonResponse(result, status=403)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@edu_pwd_error_handler
@customized_login_required
def operate_selecting_public_courses(request, number):
    """
    操作选课列表
    :param request:POST PUT DELETE
    :param number:
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'POST':
        error_code, is_success = student.add_public_elective_course_and_submit(number)
        if is_success:
            status['message'] = '新增用户已选公选课成功'
            return JsonResponse(result, status=201)
        else:
            status['code'] = error_code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
            return JsonResponse(result, status=400)
    elif request.method == 'DELETE':
        error_code, is_success = student.delete_selection_course(number)
        if is_success:
            status['message'] = '删除用户已选公选课成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = error_code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
            return JsonResponse(result, status=403)
    elif request.method == 'PUT':
        if not request.body:
            request_data = {}
        else:
            request_data = json.loads(request.body)
        is_circulate = request_data.get('is_circulate')
        if is_circulate:
            error_code, is_success = student.add_circulate_selection_courses(number)
            if is_success:
                status['message'] = "添加进无限选课列表成功"
                return JsonResponse(result, status=200)
            else:
                status['code'] = error_code
                status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
                return JsonResponse(result, status=400)
        else:
            error_code, is_success = student.delete_circulate_selection_courses(course_number_to_delete=number)
            if is_success:
                status['message'] = "从无限选课列表移除成功"
                return JsonResponse(result, status=200)
            else:
                status['code'] = error_code
                status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
                return JsonResponse(result, status=400)
    else:
        # http方法不支持
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(10)
        return JsonResponse(result, status=405)


@customized_login_required
def get_user_professional_course(request):
    """
    获取用户专业课列表
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': '获取用户专业课列表成功'}
    result = {'status': status}
    student = request.user.student
    result['professional'] = student.get_all_specialized_course()
    return JsonResponse(result, status=200)


@customized_login_required
def get_all_recommend_courses_detail(request):
    """
    返回所有推荐的专业课
    :param request: GET
    :return:
    """
    status = {'code': 0, 'message': '获取用户专业课推荐列表成功'}
    result = {'status': status, 'recommended': ''}
    student = request.user.student
    result['recommended'] = student.get_all_recommended_specialized_course()
    return JsonResponse(result, status=200)


@customized_login_required
def recommend_course(request, number):
    """
    新增或删除一门推荐专业课
    :param request: POST DELETE
    :param number: 专业课课头号
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'POST':
        error_code, is_success = student.recommend_specialized_course(int(number))
        if is_success:
            status['message'] = '推荐专业课成功'
            return JsonResponse(result, status=201)
        else:
            status['code'] = error_code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
            return JsonResponse(result, status=403)
    elif request.method == 'DELETE':
        error_code, is_success = student.delete_recommended_specialized_course(int(number))
        if is_success:
            status['message'] = '取消推荐专业课成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = error_code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
            return JsonResponse(result, status=403)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=10)
        return JsonResponse(result, status=405)


def get_all_pop_up_notice(request):
    """
    获取用户所有的弹出提示，已读或不再弹出的过滤在model层中
    :param request:
    :return:
    """
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    if request.method == 'GET':
        if request.user.is_authenticated():
            # 登录用户获取弹窗提示
            student = request.user.student
            result['prompt'] = student.get_all_pop_up_notice()
            status['message'] = '获取弹窗提示列表成功'
            return JsonResponse(result, status=200)
        else:
            # 未登录用户获取弹窗提示
            result['prompt'] = PopUpNotice.get_pop_up_notice_for_anonymous_user()
            status['message'] = '获取弹窗提示列表成功'
            return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=10)
        return JsonResponse(result, status=405)


@customized_login_required
def mark_notice_as_read(request, notice_id):
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'DELETE':
        if request.body:
            request_data = json.loads(request.body)
            do_not_remind = not request_data.get('is_repeated', True)
        else:
            do_not_remind = False
        error_code, success = student.mark_notice_as_read(notice_id=notice_id, do_not_remind=do_not_remind)
        if success:
            status['message'] = '删除弹窗提示成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = error_code
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=error_code)
            return JsonResponse(result, status=400)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=10)
        return JsonResponse(result, status=405)


@customized_login_required
def operate_sms_service(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'status': status}
    student = request.user.student
    if request.method == 'GET':
        if student.sms_service:
            status['message'] = '成功获取短信通知开通状态和用于短信通知的手机号'
            result['contact_number'] = str(student.contact_number)
            result['sms_service'] = True
            return JsonResponse(result, status=200)
        else:
            status['message'] = '成功获取短信通知开通状态'
            result['sms_service'] = False
            return JsonResponse(result, status=200)
    elif request.method == 'PUT':
        request_data = json.loads(request.body)
        contact_number = request_data.get('contact_number')
        success = request.user.student.set_contact_number(contact_number=contact_number)
        if success:
            student.enable_sms_service()
            status['message'] = '成功开通短信通知和设置短信通知手机号'
            result['contact_number'] = contact_number
            result['sms_service'] = True
            return JsonResponse(result, status=200)
        else:
            # 手机格式不正确，只支持11为的大陆手机
            status['code'] = 13
            status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=13)
            return JsonResponse(result, status=400)
    elif request.method == 'DELETE':
        student.disable_sms_service()
        status['message'] = '成功关闭短信通知'
        return JsonResponse(result, status=200)
    else:
        status['code'] = 10
        status['message'] = ErrorCodeToMessage.cast_error_code_to_message_for_front_end(error_code=10)
        return JsonResponse(result, status=405)
