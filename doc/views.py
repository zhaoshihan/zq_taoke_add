from django.shortcuts import render_to_response, HttpResponse

from zq_taoke_rebuild.settings import DEBUG

# Create your views here.


def index(request):
    if DEBUG:
        return render_to_response('doc.html', {})
    else:
        return HttpResponse(status=404)
