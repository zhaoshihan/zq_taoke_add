FORMAT: 1A
HOST: http://taoke.ziqiang.studio/api

# zq_taoke API
淘课啦API文档

文档Response中只有两个出现错误时的样例
Response的[Http Status Code](http://www.restapitutorial.com/httpstatuscodes.html)需要与当前情况相符或近似

Data Structures

+ TimeSection (object)
上课时间
    + `location`: 3区,附3-402 (string, required) - 上课地点

    + `weekday`: 2 (number, required) - 星期几上课，周一到周日对应1-7

    + `class_begin`: 3 (number, required) - 上课开始节数，取值[1-13]，≤class_over

    + `class_over`: 5 (number, required) - 上课结束节数，取值[1-13]，≥class_begin
    
    + `repeat`: 1 (number, required) - 每[]周上一次课

+ CurriculumCourse (object)
课表课程

    + `name`: 导论 (string, required) - 课程名

    + `number`: 20151391133 (number, required) - 课头号

    + `location`: 3区,附3-402 (string, required) - 上课地点

    + `weekday`: 2 (number, required) - 星期几上课，周一到周日对应1-7

    + `class_begin`: 3 (number, required) - 上课开始节数，取值[1-13]，≤class_over

    + `class_over`: 5 (number, required) - 上课结束节数，取值[1-13]，≥class_begin

    + `is_public_elective`: true (boolean, required) - 是否为公选课

+ Student (object)
学生

    + `name`: 张三 (string, required) - 姓名

    + `avatar`: /static/*** (string, required) - 头像url

+ StudentProfessionalItem (object)
专业课, key为学年，value为该学年所上的专业课的课头号
如果该学年没有专业课的话就不要返回了

    + `大一`: 20151391133, 201514202323 (array[string], optional)
    
+ PublicElectiveCourseAbstractWithOtherCourses (object)

    + number: 20161040655 (number, required) - 课头号

    + `other_courses` (array[object])
    不同时间的相同课程列表，不包括当前这门课(PublicElectiveCourseAbstract)
    
    + name: 河流概论 (string, required) - 课程名
    
    + left: 1 (number, required) - 剩余人数
    
    + total: 100 (number, required) - 总人数
    
    + teacher: 卢新华 (string, required) - 授课教师
    
    + location: 水利水电学院, 2区,11-201 (string, required) - 上课地点
    
    + time (array[object], required) - 上课的时间(TimeSection)
    
    + credit: 1 (number, required) - 学分
    
    + summary: 自然计算方法... (string, required) - 课程简介
    
    + scores (object, required) - 该课程获得的评分列表

        - attendance: 1, 2, 3 (array[number])
        考勤力度
        [ 毫无压力, 偶尔点点, 逢课必点 ]

        - quality: 1, 3, 1 (array[number])
        授课质量
        [ 引人入胜, 中规中矩, 照本宣科 ]
    
        - score: 1, 1, 3 (array[number])
        给分情况
        [ 90-100, 80-90, 0-80 ]
        
+ PublicElectiveCourseAbstract (object)

    + number: 20161040655 (number, required) - 课头号

    + name: 河流概论 (string, required) - 课程名
    
    + left: 1 (number, required) - 剩余人数
    
    + total: 100 (number, required) - 总人数
    
    + teacher: 卢新华 (string, required) - 授课教师
    
    + location: 2区,11-201 (string, required) - 上课地点
    
    + time (array[object], required) - 上课的时间(TimeSection)
    
    + credit: 1 (number, required) - 学分
    
    + summary: 自然计算方法... (string, required) - 课程简介
    
    + scores (object, required) - 该课程获得的评分列表

        - attendance: 1, 2, 3 (array[number])
        考勤力度
        [ 毫无压力, 偶尔点点, 逢课必点 ]

        - quality: 1, 3, 1 (array[number])
        授课质量
        [ 引人入胜, 中规中矩, 照本宣科 ]
    
        - score: 1, 1, 3 (array[number])
        给分情况
        [ 90-100, 80-90, 0-80 ]



+ PublicElectiveComment (object)
公选课评论

    + `student` (Student, required) - 评论人

    + `time`: 2016年1月8日 20:22 (string, required) - 评论时间

    + `content`: 很棒哒 (string, required) - 评论内容

    + `score` (PublicElectiveCommentScore, required) - 评分

+ PublicElectiveCommentScore (object)
公选课评分(分数取值{ bad, medium, good })

    + `attendance`: bad (string, required)

        考勤力度

        bad - 逢课必点

        medium - 偶尔点点

        good - 毫无压力

    + `quality`: medium (string, required)

        授课质量

        bad - 照本宣科

        medium - 中规中矩

        good - 引人入胜

+ PublicElectiveSelected (object)
已选过的公选课(按领域分)

    + `field`: 数学与推理类 (string, required) - 领域

    + `credit`: 2 (number, required) - 该领域所选过的公选课的学分总和

    + `courses` (array[object], required) - 该领域所选过的公选课列表(PublicElectiveSelectedItem)

+ PublicElectiveSelectedItem (object)
已选过的公选课信息

    + `name`: 电子商务 (string, required) - 课程名

    + `number`: 20151391133 (number, required) - 课头号

    + `credit`: 1 (number, required) - 该课程学分

+ PublicElectiveLeaderBoard (object)
公选课排行榜

    + `score` (array[object], required) - 给分排行榜(PublicElectiveCourseAbstractWithOtherCourses)

    + `quality` (array[object], required) - 授课排行榜(PublicElectiveCourseAbstractWithOtherCourses)

    + `attendance` (array[object], required) - 考勤排行榜(PublicElectiveCourseAbstractWithOtherCourses)

    + `boy` (array[object], optional)
    男生排行榜(PublicElectiveCourseAbstractWithOtherCourses)
    未登录或未评价完所有课程不返回

    + `girl` (array[object], optional)
    女生排行榜(PublicElectiveCourseAbstractWithOtherCourses)
    未登录或未评价完所有课程不返回

+ Department (object)
学部

    + `department`: 理学部 (string, required) - 学部名

    + `academies`: 数学院, 物院 (array[string], required) - 所有学院列表

+ SourceCategory (object)
公共资源大类

    + `id`: 2 (string, required) - 大类id

    + `name`: 考级类 (string, required) - 大类名

    + `icon_path`: /static/*** (string, required) - 对应图标url

    + `subcategories`: (SourceSubcategory, required) - 对应小类列表

+ SourceSubcategory (object)
公共资源小类

    + `id`: 3 (string, required) - 小类id

    + `name`: 四级 (string, required) - 小类名

    + `empty_notice`: 空白文案 (string, optional) - 当资源为空时的显示文案

+ SourceItem (object)
公共资源

    + `id`: 12 (string, required) - 公共资源id

    + `sig`: **** (string, required) - 签名

    + `exp_time`: *** (string, required) - 过期时间

    + `name`: 四级资料 (string, required) - 资源名

    + `download_count`: 3202 (number, required) - 下载次数

    + `icon_path`: /static/*** (string, required) - 对应图标url

+ Pagination (object)
分页相关参数
根据接口request的per_page计算出num_pages
同时判断request的page是否合法
比如在公选课列表的接口里面，由于筛选参数的变化有可能导致request传的page不合法
request的page > num_pages 就传最后一页
request的page < 1         就传第一页

    + `page`: 1 (number, required) - 返回的结果的页数(第一页为1)

    + `num_pages`: 10 (number, required) - 一共有多少页

+ PromptItem (object)
弹窗提示

    + `id`: 1 (number, required) - id

    + `is_repeated` (boolean, optional) - 是否是多次提醒的弹窗

        + Default: false

    + `text_title`(string, optional) - 弹窗标题文本

        + Default: 提醒

    + `text_understand`(string, optional) - 弹窗按钮文本

        + Default: 我明白了

    + `content`(string, required) - 具体内容

# zq_taoke API Root [/]
淘课啦API入口

## API [GET]

+ Response 204

# Group 权限
网站权限相关

## 登陆 [/auth/login]

### 获取用户登陆状态 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功获取用户登陆状态 (string, required) - 状态信息

        + `has_logged_in`: true (boolean, required) - 是否已经登陆过

        + `has_used_sms`: false (boolean, required) - 是否开通了短信提醒

        + number: 2015301500174 (string, optional) - 已登陆的学号，未登录不返回

### 用户登陆 [POST]

+ Request (application/json)

    + Attributes

        + number: 2015301500174 (string, required) - 学号

        + password: hhasd (string, required) - 密码

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 登录成功 (string, required) - 状态信息

        + `has_used_sms`: false (boolean, required) - 是否开通了短信提醒

+ Response 403 (application/json)

    + Body

            {
                "status": {
                    "code": *,
                    "message": "学号或密码错误对应信息"
                }
            }

## 登出 [/auth/logout]

### 用户登出 [GET]

+ Response 200 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "登出成功"
                }
            }

# Group 用户
用户相关

## 用户信息 [/student]

### 获取用户信息 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功获取用户信息 (string, required) - 状态信息

        + name: 哈哈哒 (string, required) - 姓名

        + gender: 男 (string, required) - 性别

        + gpa: 4.4 (string, optional) - gpa，获取出错时不返回

        + avatar: /static/*** (string, required) - 头像url

        + academy: 计算机学院 (string, required) - 所在学院

+ Response 401 (application/json)

    + Body

            {
                "status": {
                    "code": *,
                    "message": "未登录",
                }
            }

## 用户头像 [/student/avatar]
获取、修改用户头像

### 获取用户头像 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户头像成功 (string, required) - 状态信息

        + avatar: /static/*** (string, required) - 头像url

### 设置随机头像 [PUT]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 设置随机头像成功 (string, required) - 状态信息

        + avatar: /static/*** (string, required) - 头像url

### 设置用户自定义头像 [POST]

+ request (multipart/form-dataform)

    + Body

            {
                "avatar": [文件二进制数据]
            }

+ Response 201 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 设置用户自定义头像成功 (string, required) - 状态信息

        + avatar: /static/*** (string, required) - 头像url

## 随机头像 [/student/avatar/random]

### 获取一张随机头像 [GET]
只返回头像url，不对用户头像进行修改

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取一张随机头像成功 (string, required) - 状态信息

        + avatar: /static/*** (string, required) - 头像url

## 用户课表 [/student/curriculum]
课程标识为课头号

### 获取用户课表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户课表成功 (string, required) - 状态信息

        + curriculum (array[object], required) - 课程列表中所有课程(CurriculumCourse)

+ Response 403 (application/json)

    + Body

            {
                "status": {
                    "code": *,
                    "message": "APi故障或教务系统故障等错误信息"
                }
            }

## 用户已选公选课列表 [/student/public/elective/selected]

### 获取用户已选公选课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户已选公选课列表成功 (string, required) - 状态信息

        + selected (object, required) - 用户已选过的公选课列表(PublicElectiveSelected)

## 用户已收藏公选课列表 [/student/public/elective/collected]

### 获取用户已收藏公选课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户已收藏公选课列表成功 (string, required) - 状态信息

        + collected (array[object], required) - 用户已收藏公选课列表中所有课程(PublicElectiveCourseAbstract)

## 用户收藏公选课 [/student/public/elective/collected/{number}]
对课头号为number的公选课进行操作

+ Parameters

    + number: 20151391133 (string) - 公选课课头号

### 新增用户收藏公选课 [POST]

+ Response 201 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "新增用户已收藏公选课成功"
                }
            }

### 删除用户已收藏公选课 [DELETE]

+ Response 200 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "删除用户已收藏公选课成功"
                }
            }

## 用户未评价公选课列表 [/student/public/elective/uncomment]

### 获取用户未评价公选课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户未评价公选课列表成功 (string, required) - 状态信息

        + uncomment (array[object], required) - 用户未评价公选课列表中所有课程(PublicElectiveCourseAbstract)

## 用户未评价公选课 [/student/public/elective/uncomment/{number}]

+ Parameters

    + number: 20151391133 (string) - 公选课课头号

### 评价用户未评价公选课 [DELETE]

+ Request (application/json)

    + Attributes (object)

        + name: 张三 (string, optional) - 评论昵称

        + score (object, required) - 评分(PublicElectiveCommentScore)

        + comment: 好好好 (string, optional) - 短评

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 评价成功 (string, required)

## 用户选课列表 [/student/public/elective/selecting]

### 获取用户选课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户选课列表成功 (string, required)
            
        + selecting (array[object], required) - 用户选课列表中所有课程(PublicElectiveCourseAbstract)

        + circulating (array[object], required) - 用户无限选课列表中所有课程(PublicElectiveCourseAbstract)
            
        + failed (array[object], required) - 用户上次选课失败列表中所有课程(PublicElectiveCourseAbstract)

## 用户选课列表中的公选课 [/student/public/elective/selecting/{number}]
对课头号为number的公选课进行操作

+ Parameters

    + number: 20151391133 (string) - 公选课课头号

### 新增用户已选公选课 [POST]

+ Request (application/json)

    + Attributes

        + is_circulate: true (boolean, optional) - 是否开启无限选课

            + Default: true

+ Response 201 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "新增用户已选公选课成功"
                }
            }

### 修改用户已选公选课状态 [PUT]

+ Request (application/json)

    + Attributes

        + is_circulate: true (boolean, optional) - 是否开启无限选课

            + Default: true

+ Response 200 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "修改用户已选公选课状态添加进无限选课列表成功"
                }
            }

### 删除用户已选公选课 [DELETE]

+ Response 200 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "删除用户已选公选课成功"
                }
            }

## 用户专业课列表 [/student/professional]

### 获取用户专业课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户专业课列表成功 (string, required) - 状态信息

        + professional (array[object], required) - 获取用户专业课列表中所有课程(StudentProfessionalItem)

## 用户专业课推荐列表 [/student/professional/recommended]

### 获取用户专业课推荐列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户专业课推荐列表成功 (string, required) - 状态信息

        + recommended: 20151391133, 201514202323 (array[string], required) - 用户专业课推荐列表中所有课程的课头号

## 用户推荐的专业课 [/student/professional/recommended/{number}]

+ Parameters

    + number: 20151391133 (string) - 公选课课头号

### 推荐专业课 [POST]

+ Response 201 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "推荐专业课成功"
                }
            }

### 取消推荐专业课 [DELETE]

+ Response 200 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "取消推荐专业课成功"
                }
            }

## 用户反馈 [/student/feedback]

### 新增反馈 [POST]

+ Request (application/json)

    + Attributes (object)

        + is_like: true (boolean, required) - 夸还是骂

        + feedback: 改改改 (string, required) - 反馈意见内容

        + contact: 18212312899 (string, optional) - 联系方式

+ Response 201 (application/json)

    + Body

            {
                "status": {
                    "code": 0,
                    "message": "反馈成功"
                }
            }

## 用户短信提醒 [/student/sms]

### 查看短信提醒开通状况 [GET]

+ Response 200 (application/json)

    + Attributes (object)
    
        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功获取短信通知开通状态和用于短信通知的手机号 (string, required)
    
        + sms_service: true (boolean, required) - 是否开通短信提醒
    
        + contact_number: 15071295301 (string, optional) - 发送短信的手机号
        
### 开通短信提醒 [PUT]

+ Request (application/json)

    + Attributes (object)
        
        + contact_number: 15071295301 (string, required) - 发送短信的手机号

+ Response 200 (application/json)

    + Attributes (object)
        
        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功开通短信通知和设置短信通知手机号 (string, required)
    
+ Response 400 (application/json)

    + Attributes (object)
        
        + status (object, required)

            - code: 13 (number, required) - 状态码

            - message: 手机格式不正确，只支持11为的大陆手机 (string, required)
            
### 关闭短信提醒 [DELETE]

+ Response 200 (application/json)

    + Attributes (object)
        
        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功关闭短信通知 (string, required)
    
# Group 课程

## 公选课列表 [/course/public/elective]

### 获取公选课列表 [POST]

+ Request (application/json)

    + Attributes (object)

        + search_key: 概论 (string, optional)
        搜索关键字，匹配课程名和教师名

        + weekday: 1, 2 (array[number], optional)
        课程时间是周几，周一到周日对应数字1-7

        + department: 1, 3 (array[number], optional)
        上课地点所在学部
        1 - 文理学部
        2 - 工学部
        3 - 信息学部
        4 - 医学部

        + field: 人文与社会类, 数学与推理类 (array[string], optional)
        所属领域
        自然与工程类
        研究与领导类
        艺术与欣赏类
        人文与社会类
        数学与推理类
        中国与全球类
        交流与写作类

        + credit: 1, 2 (array[number], optional)
        课程学分

        + class_time: morning (string, optional)
        上课时间的节数范围
        morning: [1, 5]
        afternoon: [6, 10]
        evening: [11, 13]

        + order: score, quality (array[string], optional)
        排序关键字(降序)
        quality - 讲课质量
        attendance - 考勤力度
        score - 给分情况
        boy - 男生数量
        girl - 女生数量

        + `only_remain`: false (boolean, optional) - 是否只返回剩余人数大于零的课程

            + default: false

        + page: 2 (number, optional) - 页码，从1开始

            + Default: 1

        + per_page: 20 (number, optional) - 一页中的结果数量，-1为一次性返回所有结果

            + Default: -1

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公选课列表成功 (string, required) - 状态信息

        + public_elective (array[object], required) - 筛选后公选课的详情(PublicElectiveCourseAbstract)

        + pagination (object, required) - 分页结果(Pagination)

## 公选课排行榜 [/course/public/elective/leader_board]

### 获取公选课排行榜 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公选课排行榜成功 (string, required) - 状态信息

        + leader_board (object, required) - 排行榜(PublicElectiveLeaderBoard)

## 热门公选课列表 [/course/public/elective/popular]

### 获取热门公选课列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取热门公选课列表成功 (string, required) - 状态信息

        + popular (array[object], required) - 热门公选课(PublicElectiveCourseAbstractWithOtherCourses)

## 公选课 [/course/public/elective/{number}]

+ Parameters

    + number: 20151391133 (number, required) - 课头号

### 获取公选课信息 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公选课信息成功 (string, required) - 状态信息
            
        + number: 20161040655 (number, required) - 课头号

        + name: 河流概论 (string, required) - 课程名

        + left: 1 (number, required) - 剩余人数

        + total: 100 (number, required) - 总人数

        + teacher: 卢新华 (string, required) - 授课教师

        + location: 水利水电学院, 2区,11-201 (string, required) - 上课地点

        + time (array[object], required) - 上课的时间(TimeSection)

        + credit: 1 (number, required) - 学分

        + summary: 自然计算方法... (string, required) - 课程简介

        + can_comment: false (boolean, required) - 能否点评这门课

        + has_selected: false (boolean, required) - 是否选过这门课

        + has_collected: true (boolean, required) - 是否已收藏

        + has_circulated: false (boolean, required) - 是否已开启无限选课

        + has_commented: false (boolean, required) - 是否评论过

        + score (string, optional)
        本课程的课程分数，未登录或没有该课程成绩则不返回

        + scores (object, required) - 该课程获得的评分列表

            - attendance: 1, 2, 3 (array[number])
            考勤力度
            [ 毫无压力, 偶尔点点, 逢课必点 ]

            - quality: 1, 3, 1 (array[number])
            授课质量
            [ 引人入胜, 中规中矩, 照本宣科 ]

            - score: 1, 1, 3 (array[number])
            给分情况
            [ 90-100, 80-90, 0-80 ]

## 公选课评论列表 [/course/public/elective/{number}/comment{?page,per_page}]

+ Parameters

    + number: 20151391133 (number, required) - 课头号

    + page: 2 (number, optional) - 页码，从1开始

        + Default: 1

    + per_page: 20 (number, optional) - 一页中的结果数量，-1为一次性返回所有结果

        + Default: -1

### 获取公选课评论列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公选课评论列表成功 (string, required) - 状态信息

        + comment (array[object], required) - 评论列表(PublicElectiveComment)

        + pagination (object, required) - 分页结果(Pagination)

## 公选课学生列表 [/course/public/elective/{number}/student{?page,per_page}]

+ Parameters

    + number: 20151391133 (number, required) - 课头号

    + page: 2 (number, optional) - 页码，从1开始

        + Default: 1

    + per_page: 20 (number, optional) - 一页中的结果数量，-1为一次性返回所有结果

        + Default: -1

### 获取公选课学生列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公选课学生列表成功 (string, required) - 状态信息

        + student (array[object], required) - 学生列表(Student)

        + pagination (object, required) - 分页结果(Pagination)

## 专业课列表 [/course/professional{?page,per_page}]

+ Parameters

    + page: 2 (number, optional) - 页码，从1开始

        + Default: 1

    + per_page: 20 (number, optional) - 一页中的结果数量，-1为一次性返回所有结果

        + Default: -1

### 获取专业课列表 [GET]

+ Request (application/json)

    + Attributes (object)

        + academy: 计算机学院 (string, optional) - 筛选专业课所属学院，可选

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取专业课列表成功 (string, required) - 状态信息

        + professional: 20151391133 (array[string], required) - 筛选过后的专业课列表

        + pagination (object, required) - 分页结果(Pagination)

## 专业课 [/course/professional/{number}]

+ Parameters

    + number: 20151391133 (number, required) - 课头号

### 获取专业课信息 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取专业课信息成功 (string, required) - 状态信息

        + name: 随机过程 (string, required) - 课程名

        + grade: 1 (number, required) - 几年级的课，1代表大一，2代表大二，以此类推

        + teacher: 李三 (string, required) - 授课老师

        + rank: 1 (number, required) - 推荐等级

        + academy: 数学与统计学院 (string, required) - 所属学院

        + time: 周3:10-16周,每1周;11-12节,2区,5-203, 周3:10-16周,每1周;11-12节,2区,5-203 (array[string], required) - 上课时间列表

## 学部 [/course/department]

### 获取学部列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取学部列表成功 (string, required) - 状态信息

        + department (array[object], required) - 学部列表(Department)

## 学院 [/course/academy]

### 获取学院列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取学部列表成功 (string, required) - 状态信息

        + department (array[object], required) - 学部列表(Department)

# group 公共资源

## 公共资源类别列表 [/source]

### 获取公共资源类别列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公共资源类别列表成功 (string, required) - 状态信息

        + categories (array[object], required) - 公共资源类别列表(SourceCategory)

## 公共资源列表 [/source/{category}/{subcategory}{?page,per_page}]

+ Parameters

    + category: 2 (string, required) - 大类id

    + subcategory: 10 (string, required) - 小类id

    + page: 2 (number, optional) - 页码，从1开始

        + Default: 1

    + per_page: 20 (number, optional) - 一页中的结果数量，-1为一次性返回所有结果

        + Default: -1

### 获取公共资源列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公共资源列表成功 (string, required) - 状态信息

        + source (array[object], required) - 公共资源列表(SourceItem)

        + pagination (object, required) - 分页结果(Pagination)

## 下载公共资源 [/sources/{category}/{subcategory}/{id}]

+ Parameters

    + category: 2 (string, required) - 大类id

    + subcategory: 10 (string, required) - 小类id

    + id: 12 (string, required) - 资源id

### 获取公共资源 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取公共资源成功 (string, required) - 状态信息

        + url: *** (string, required) - 下载链接

# Group 弹窗提示

## 弹窗提示列表 [/prompt]

### 获取弹窗提示列表 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取弹窗提示列表成功 (string, required) - 状态信息

        + prompt (array[object], required) - 弹窗提示列表(PromptItem)

## 弹窗提示 [/prompt/{id}]

+ Parameters

    + id: 12 (number, required) - 资源id

### 删除提示 [DELETE]
看过提示后，调用此接口，以后就不用弹出了

+ Request (application/json)

    + Attributes (object)

        + is_repeated: true (boolean, optional)
        若为false则下次不再提醒

            + Default: true

+ Response 200 (application/json)

    + Attributes (object)

        + status (object, required)

            - code: 0 (number, required) - 状态码
