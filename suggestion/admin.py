# -*- coding: utf-8 -*-

from django.contrib import admin
from suggestion.models import *


# Register your models here.


@admin.register(Suggestion)
class SuggestionAdmin(admin.ModelAdmin):
    list_display = ('like', 'suggestion', 'published', 'student', 'gender', 'number', 'name', 'academic', 'contact',)
    search_fields = ('name', 'number',)


@admin.register(ErrorCodeToMessage)
class ErrorCodeToMessageAdmin(admin.ModelAdmin):
    list_display = ('code', 'message')
    search_fields = ('code',)


@admin.register(PopUpNotice)
class PopUpNoticeAdmin(admin.ModelAdmin):
    list_display = ('is_disabled', 'pop_up_url', 'is_repeated', 'pop_for_anonymous_user', 'title', 'understand', 'content', 'rank')
    list_filter = ('is_disabled', 'is_repeated', 'pop_for_anonymous_user',)
