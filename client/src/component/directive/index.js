
import { eventPathIncludes } from "util";

export const FlexImg = {
    bind() {
        const el = this.el;
        const char = this.arg || "$";
        const dpr = Number(document.querySelector("html").getAttribute("data-dpr") || 1);
        const min = Number(el.getAttribute("min") || 0);
        let oldScale = -1;

        this.isReady = false;
        this.updateSrc = () => {
            const width = el.offsetWidth;
            let scale = Math.ceil(width * dpr / min);
            if (scale < 1) {
                scale = 1;
            }
            else if (scale > 3) {
                scale = 3;
            }

            if (scale !== oldScale) {
                oldScale = scale;
                el.setAttribute("src", this.value.replace(char, scale));
            }
        };

        const interval = setInterval(() => {
            if (el.offsetParent) {
                clearInterval(interval);
                this.isReady = true;
                if (this.value) {
                    this.updateSrc();
                }
            }
        }, 50);
    },
    update(value) {
        this.value = value;
        if (this.isReady) {
            this.updateSrc();
        }
    },
};

export const Scroll = {
    bind() {
        const el = this.el;
        const threshold = Number(el.getAttribute("threshold") || 40);
        document.addEventListener("scroll", (event) => {
            const {
                scrollTop,
                offsetHeight,
                scrollHeight,
            } = el;

            if (!!this.value && eventPathIncludes(event, el) &&
                (scrollTop + offsetHeight + threshold >= scrollHeight)) {
                this.value();
            }
        }, true);
    },
    update(value) {
        if (value instanceof Function) {
            this.value = value;
        }
        else {
            this.value = null;
        }
    },
};
