import Vue from "vue";
import VueRouter from "vue-router";

// import Promise from "promise";

import * as config from "config";

import App from "container/pc/App";

// install plugin
Vue.use(VueRouter);

// define router
const router = new VueRouter(config.router);

// define router map rules
router.map({
    "/error": {
        name: "Error",
        isError: true,
        component: require("container/pc/Error"),
    },
});

// define router redirect rules
router.redirect({
    "*": "/error",
});

router.start(App, "#app");
