import Vue from "vue";
import VueRouter from "vue-router";

import Promise from "promise";

import * as config from "config";

import App from "container/pc/App";

// install plugin
Vue.use(VueRouter);

// define router
const router = new VueRouter(config.router);

// define router map rules
router.map({
    "/student": {
        component: require("container/pc/Student"),
        loginRequired: true,
        subRoutes: {
            "/curriculum": {
                name: "StudentCurriculum",
                component: require("container/pc/StudentCurriculum"),
            },
            "/collected": {
                name: "StudentCollected",
                component: require("container/pc/StudentCollected"),
            },
            "/selecting": {
                name: "StudentSelecting",
                component: require("container/pc/StudentSelecting"),
            },
            "/professional": {
                name: "StudentProfessional",
                component: require("container/pc/StudentProfessional"),
            },
            "/market": {
                name: "StudentMarket",
                component: require("container/pc/StudentMarket"),
            },
        },
    },
    "/course/public/elective": {
        name: "CoursePublicElective",
        component: require("container/pc/CoursePublicElective"),
    },
    "/course/public/elective/:number": {
        name: "CoursePublicElectiveDetail",
        component: require("container/pc/CoursePublicElectiveDetail"),
    },
    "/course/professional": {
        name: "CourseProfessional",
        component: require("container/pc/CourseProfessional"),
    },
    "/source": {
        name: "Source",
        loginRequired: true,
        component: require("container/pc/Source"),
    },
    "/login": {
        name: "Login",
        auth: true,
        component: require("container/pc/Login"),
    },
    "/error": {
        name: "Error",
        isError: true,
        component: require("container/pc/Error"),
    },
});

// define router redirect rules
router.redirect({
    "/course": "/course/public/elective",
    "/student": "/student/curriculum",
    "*": "/student",
});

router.beforeEach(
    ({ to }) => to.router.app.getLoginStatus().then(() => {
        const { hasLoggedIn } = to.router.app;
        if (!hasLoggedIn) {
            if (!!to.loginRequired) {
                to.router.go({
                    path: "/login",
                    query: {
                        next: to.path,
                    },
                });
            }
        }
        else if (!!to.auth) {
            to.router.go("/student");
        }
        // else if (to.path === "/student") {
        //     const { number } = studentAccount;
        //     const currentYear = new Date().getFullYear();
        //     if (number.toString().indexOf(currentYear.toString()) === 0) {
        //         to.router.go("/student/curriculum");
        //     }
        //     else {
        //         to.router.go("/student/professional");
        //     }
        // }

        return Promise.resolve(true);
    })
);

router.start(App, "#app");
