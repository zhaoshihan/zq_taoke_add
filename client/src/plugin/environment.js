const doc = window.document;
const html = doc.documentElement;
const navigator = window.navigator;
const appVersion = navigator.appVersion;

export const isAndroid = window.isAndroid = appVersion.match(/android/gi);
export const isIPhone = window.isIPhone = appVersion.match(/iphone/gi);

html.setAttribute("data-device", isIPhone ? "iphone" : "other");
