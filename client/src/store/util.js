export function getPublicElectiveList(state, numbers) {
    if (numbers instanceof Array) {
        const courses = state.publicElective.all;
        return numbers.map(number => courses[number])
            .filter(course => !!course);
    }
    return [];
}

export function getProfessionalList(state, numbers) {
    if (numbers instanceof Array) {
        const courses = state.professional.all;
        return numbers.map(number => courses[number])
            .filter(course => !!course);
    }
    return [];
}

export function hasLoadedSourceCategory(state, params = {}, page) {
    const all = state.source.all;
    const { category, subcategory } = params;
    if ((category !== null) && (subcategory !== null) &&
        (typeof page === "number") && !!all[category] &&
        !!all[category][subcategory]) {
        const { list } = all[category][subcategory];
        return !!list[page];
    }
    return false;
}
