import {
    getPublicElectiveList,
    getProfessionalList,
} from "./util";

export function studentProfile(state) {
    return (state.student.profile || {});
}

export function studentAccount(state) {
    return (state.student.account || {});
}

export function studentCurriculum(state) {
    return (state.student.curriculum || []);
}

export function studentSelected(state) {
    return (state.student.selected || []);
}

export function studentUncomment(state) {
    return getPublicElectiveList(state, state.student.uncomment);
}

export function studentCollected(state) {
    return getPublicElectiveList(state, state.student.collected);
}

export function studentSelecting(state) {
    return getPublicElectiveList(state, state.student.selecting);
}

export function studentSelectingFailed(state) {
    return getPublicElectiveList(state, state.student.selectingFailed);
}

export function studentCirculating(state) {
    return getPublicElectiveList(state, state.student.circulating);
}

export function studentProfessional(state) {
    const professional = state.student.professional || {};
    const grades = Object.keys(professional);
    const obj = {};
    grades.forEach(
        grade => {
            const list = getProfessionalList(state, professional[grade]);
            if (list.length > 0) {
                obj[grade] = list;
            }
        }
    );
    return obj;
}

export function studentProfessionalRecommended(state) {
    return getProfessionalList(state, state.student.professionalRecommended);
}

export function studentSms(state) {
    return (state.student.sms || {});
}

export function publicElectiveIsLoading(state) {
    return state.publicElective.isLoading;
}

export function publicElectiveAll(state) {
    return state.publicElective.all;
}

export function publicElectivePagination(state) {
    return state.publicElective.pagination;
}

export function publicElectiveParams(state) {
    return state.publicElective.params;
}

export function publicElectiveCurrentCourseList(state) {
    const courses = state.publicElective;
    const { current } = courses;
    const { page } = courses.pagination;
    const list = current[page] || [];
    return getPublicElectiveList(state, list);
}

export function publicElectiveCurrentAllCourseList(state) {
    const courses = state.publicElective;
    const { current } = courses;
    const list = current.reduce(
        (prev, cur) => {
            if (cur instanceof Array) {
                prev.push(...cur);
            }
            return prev;
        },
        []
    );
    return getPublicElectiveList(state, list);
}

export function publicElectiveStudentPagination(state) {
    return state.publicElective.students.current.pagination;
}

export function publicElectiveStudentList(state) {
    const { list, pagination } = state.publicElective.students.current;
    const { page } = pagination;
    return (list[page] || []);
}

export function publicElectiveCommentPagination(state) {
    return (state.publicElective.comments.current.pagination);
}

export function publicElectiveCommentList(state) {
    const { list, pagination } = state.publicElective.comments.current;
    const { page } = pagination;
    return (list[page] || []);
}

export function publicElectiveCommentAllList(state) {
    const { list } = state.publicElective.comments.current;
    const ret = list.reduce(
        (prev, cur) => {
            if (cur instanceof Array) {
                prev.push(...cur);
            }
            return prev;
        },
        []
    );
    return ret;
}

export function publicElectiveLeaderBoard(state) {
    const { leaderBoard } = state.publicElective;
    const ret = {};
    Object.keys(leaderBoard || {}).forEach(key => (
        ret[key] = getPublicElectiveList(state, leaderBoard[key])
    ));
    return ret;
}

export function publicElectivePopular(state) {
    return getPublicElectiveList(state, state.publicElective.popular || []);
}

export function professionalPagination(state) {
    return state.professional.pagination;
}

export function professionalParams(state) {
    return state.professional.params;
}

export function professionalCurrentCourseList(state) {
    const courses = state.professional;
    if (!!courses.current) {
        const { page } = courses.pagination;
        return getProfessionalList(state, courses.current[page] || []);
    }
    return [];
}

export function department(state) {
    return (state.department.list || []);
}

export function source(state) {
    return (state.source.categories || []);
}

export function sourceParams(state) {
    return state.source.params;
}

export function sourceCategoryPagination(state) {
    return state.source.current.pagination;
}

export function sourceCategoryList(state) {
    return state.source.current.list;
}

export function promptList(state) {
    return (state.prompt.list || []);
}

export function hasShowedSearch(state) {
    return state.webapp.search;
}

export function hasShowedFilter(state) {
    return state.webapp.filter;
}

export function hasShowedPopup(state) {
    return (state.pc ? state.pc.popup.enable : state.webapp.popup.enable);
}

export function popup(state) {
    return (state.pc ? state.pc.popup : state.webapp.popup);
}

export function hasLoggedIn(state) {
    return state.student.status.hasLoggedIn;
}
