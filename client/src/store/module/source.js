import {
    RECEIVE_SOURCE,
    RECEIVE_SOURCE_CATEGORY,
    UPDATE_SOURCE_CATEGORY_CURRENT,
} from "../mutation-type";

// initial state
const state = {
    params: {
        category: null,
        subcategory: null,
        emptyNotice: null,
    },
    categories: null,
    current: {
        list: [],
        pagination: {
            page: 1,
            numPages: 0,
        },
    },
    all: {},
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [RECEIVE_SOURCE](state, categories) {
        state.categories = categories;
    },

    [RECEIVE_SOURCE_CATEGORY](state, sources, { params, pagination }) {
        const all = state.all;
        const { category, subcategory } = params;
        const { page = 1 } = pagination;
        if ((category && subcategory) !== null) {
            if (!all[category]) {
                all[category] = {};
            }
            if (!all[category][subcategory]) {
                all[category][subcategory] = {
                    list: [],
                    pagination,
                };
            }

            const obj = all[category][subcategory];
            obj.list[page] = sources;
            obj.pagination = pagination;
        }
    },

    [UPDATE_SOURCE_CATEGORY_CURRENT](state, { params, pagination: pg } = {}) {
        state.params = Object.assign(state.params, params || {});

        const { category, subcategory } = state.params;
        const { page } = pg || {};
        const { list, pagination } = state.all[category][subcategory];
        Object.assign(pagination, { page });
        state.current = {
            list: list[page],
            pagination,
        };
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
