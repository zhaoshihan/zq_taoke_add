import {
    UPDATE_STUDENT_STATUS,
    RECEIVE_STUDENT_ACCOUNT,
    RECEIVE_STUDENT_PROFILE,
    UPDATE_STUDENT_PROFILE,
    RECEIVE_STUDENT_CURRICULUM,
    RECEIVE_STUDENT_SELECTED,
    RECEIVE_STUDENT_UNCOMMENT,
    RECEIVE_STUDENT_COLLECTED,
    SAVE_STUDENT_COLLECTED_ITEM,
    DELETE_STUDENT_COLLECTED_ITEM,
    RECEIVE_STUDENT_SELECTING,
    SAVE_STUDENT_SELECTING_ITEM,
    DELETE_STUDENT_SELECTING_ITEM,
    RECEIVE_STUDENT_SELECTING_FAILED,
    RECEIVE_STUDENT_CIRCULATING,
    SAVE_STUDENT_CIRCULATING_ITEM,
    DELETE_STUDENT_CIRCULATING_ITEM,
    DELETE_STUDENT_UNCOMMENT_ITEM,
    RECEIVE_STUDENT_PROFESSIONAL,
    RECEIVE_STUDENT_PROFESSIONAL_RECOMMENDED,
    SAVE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM,
    DELETE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM,
    UPDATE_STUDENT_SMS,
} from "../mutation-type";

// initial state
const state = {
    status: {
        hasLoggedIn: null,
        hasSawGuide: null,
    },
    // shape: { number, password }
    account: null,
    // shape: { name, gender, gpa, avatar, academy }
    profile: null,
    selected: null,
    selecting: null,
    selectingFailed: null,
    circulating: null,
    collected: null,
    credit: null,
    uncomment: null,
    professional: null,
    professionalRecommended: null,
    // item shape: { name, location, weekday, classBegin, classOver, isPublicElective }
    curriculum: null,
    sms: null,
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [UPDATE_STUDENT_STATUS](state, status) {
        state.status = Object.assign(state.status, status);
        if (!status.hasLoggedIn) {
            state.account = null;
        }
    },

    [RECEIVE_STUDENT_ACCOUNT](state, account) {
        state.account = account;
    },

    [RECEIVE_STUDENT_PROFILE](state, profile) {
        state.profile = profile;
    },

    [UPDATE_STUDENT_PROFILE](state, profile) {
        state.profile = Object.assign(state.profile || {}, profile);
    },

    [RECEIVE_STUDENT_CURRICULUM](state, curriculum) {
        state.curriculum = curriculum;
    },

    [RECEIVE_STUDENT_SELECTED](state, selected) {
        state.selected = selected;
    },

    [RECEIVE_STUDENT_UNCOMMENT](state, uncomment) {
        state.uncomment = uncomment;
    },

    [RECEIVE_STUDENT_COLLECTED](state, collected) {
        state.collected = collected;
    },

    [SAVE_STUDENT_COLLECTED_ITEM](state, number) {
        const { collected } = state;
        if (!!collected) {
            collected.push(number);
        }
    },

    [DELETE_STUDENT_COLLECTED_ITEM](state, number) {
        const { collected } = state;
        if (!!collected) {
            state.collected = collected.filter(
                _number => (_number !== number)
            );
        }
    },

    [RECEIVE_STUDENT_SELECTING](state, selecting) {
        state.selecting = selecting;
    },

    [SAVE_STUDENT_SELECTING_ITEM](state, number) {
        const { selecting } = state;
        if (!!selecting && !selecting.includes(number)) {
            selecting.push(number);
        }
    },

    [DELETE_STUDENT_SELECTING_ITEM](state, number) {
        const { selecting } = state;
        if (!!selecting) {
            state.selecting = selecting.filter(
                _number => (_number !== number)
            );
        }
    },

    [RECEIVE_STUDENT_SELECTING_FAILED](state, failed) {
        state.selectingFailed = failed;
    },

    [RECEIVE_STUDENT_CIRCULATING](state, circulating) {
        state.circulating = circulating;
    },

    [SAVE_STUDENT_CIRCULATING_ITEM](state, number) {
        const { circulating } = state;
        if (!!circulating && !circulating.includes(number)) {
            circulating.push(number);
        }
    },

    [DELETE_STUDENT_CIRCULATING_ITEM](state, number) {
        const { circulating } = state;
        if (!!circulating) {
            state.circulating = circulating.filter(
                _number => (_number !== number)
            );
        }
    },

    [DELETE_STUDENT_UNCOMMENT_ITEM](state, number) {
        const { uncomment } = state;
        if (!!uncomment) {
            state.uncomment = uncomment.filter(
                _number => (_number !== number)
            );
        }
    },

    [RECEIVE_STUDENT_PROFESSIONAL](state, professional) {
        state.professional = professional;
    },

    [RECEIVE_STUDENT_PROFESSIONAL_RECOMMENDED](state, professionalRecommended) {
        state.professionalRecommended = professionalRecommended;
    },

    [SAVE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM](state, number) {
        const recommended = state.professionalRecommended;
        if (!!recommended) {
            recommended.push(number);
        }
    },

    [DELETE_STUDENT_PROFESSIONAL_RECOMMENDED_ITEM](state, number) {
        const recommended = state.professionalRecommended;
        if (!!recommended) {
            state.professionalRecommended = recommended.filter(
                _number => (_number !== number)
            );
        }
    },

    [UPDATE_STUDENT_SMS](state, sms) {
        state.sms = Object.assign({}, state.sms, sms);
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
