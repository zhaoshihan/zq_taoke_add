import Vue from "vue";

import {
    START_PUBLIC_ELECTIVE_LOADING,
    END_PUBLIC_ELECTIVE_LOADING,
    RESET_PUBLIC_ELECTIVE_ALL,
    UPDATE_PUBLIC_ELECTIVE_CURRENT,
    RECEIVE_PUBLIC_ELECTIVE_ITEM,
    RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM,
    UPDATE_PUBLIC_ELECTIVE_ITEM,
    RECEIVE_PUBLIC_ELECTIVE_STUDENT_ITEM,
    UPDATE_PUBLIC_ELECTIVE_STUDENT_PAGINTION,
    UPDATE_PUBLIC_ELECTIVE_STUDENT_CURRENT,
    RECEIVE_PUBLIC_ELECTIVE_COMMENT_ITEM,
    UPDATE_PUBLIC_ELECTIVE_COMMENT_PAGINTION,
    UPDATE_PUBLIC_ELECTIVE_COMMENT_CURRENT,
    RECEIVE_PUBLIC_ELECTIVE_LEADER_BOARD,
    RECEIVE_PUBLIC_ELECTIVE_POPULAR,
} from "../mutation-type";

// initial state
const state = {
    isLoading: false,
    pagination: {
        page: 0,
        numPages: null,
    },
    params: {
        // filter
        searchKey: null,
        hasRemain: null,
        classTime: null,
        weekday: null,
        department: null,
        field: null,
        credit: null,

        // default: sort descending
        order: null,
    },
    current: [],
    popular: null,
    // key - course number
    // value - {
    //      isAbstract,
    //      name,
    //      left,
    //      total,
    //      teacher,
    //      weekday,
    //      weekFrom,
    //      weekTo,
    //      repeat,
    //      classBegin,
    //      classOver,
    //      credit,
    //      summary,
    //      hasCollected,
    //      hasCirculated,
    //      score
    //      scores - { attendance, quality, score }
    // }
    all: {},
    comments: {
        current: {
            list: [],
            pagination: {
                page: 0,
                numPages: null,
            },
        },
        all: {},
    },
    students: {
        current: {
            list: [],
            pagination: {
                page: 0,
                numPages: null,
            },
        },
        all: {},
    },
    leaderBoard: null,
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [START_PUBLIC_ELECTIVE_LOADING](state) {
        state.isLoading = true;
    },

    [END_PUBLIC_ELECTIVE_LOADING](state) {
        state.isLoading = false;
    },

    [RESET_PUBLIC_ELECTIVE_ALL](state) {
        state.current = [];
        state.all = {};
        state.pagination = {
            page: 0,
            numPages: null,
        };

        const { params } = state;
        Object.keys(params).forEach(key => Vue.set(params, key, null));
    },

    [UPDATE_PUBLIC_ELECTIVE_CURRENT](state, { params, pagination }, courses) {
        if (!!params) {
            state.current = [];
            // state.params = Object.assign(
            //     {},
            //     state.params,
            //     params
            // );
            state.params = params;
        }

        const { page } = state.pagination = Object.assign(
            {},
            state.pagination,
            pagination
        );

        if (!!courses) {
            Vue.set(state.current, page, courses);
        }
    },

    [RECEIVE_PUBLIC_ELECTIVE_ITEM](state, number, course) {
        const { time } = course;
        if ((time instanceof Array) && (time.length > 0)) {
            Object.assign(course, time[0]);
        }
        if (course.otherCourses instanceof Array) {
            course.otherCourses.forEach(course => {
                const { time } = course;
                if ((time instanceof Array) && (time.length > 0)) {
                    Object.assign(course, time[0]);
                }
            });
        }
        Vue.set(state.all, number, course);
    },

    [RECEIVE_PUBLIC_ELECTIVE_ABSTRACT_ITEM](state, number, course) {
        if (!!state.all[number]) {
            return;
        }

        const { time } = course;
        if ((time instanceof Array) && (time.length > 0)) {
            Object.assign(course, time[0]);
        }
        if (course.otherCourses instanceof Array) {
            course.otherCourses.forEach(course => {
                const { time } = course;
                if ((time instanceof Array) && (time.length > 0)) {
                    Object.assign(course, time[0]);
                }
            });
        }
        Object.assign(course, { isAbstract: true });
        Vue.set(state.all, number, course);
    },

    [UPDATE_PUBLIC_ELECTIVE_ITEM](state, number, _course) {
        if (!state.all[number]) {
            state.all[number] = {};
        }
        Vue.set(
            state.all,
            number,
            Object.assign(state.all[number], _course)
        );
    },

    [RECEIVE_PUBLIC_ELECTIVE_STUDENT_ITEM](state, number, pagination, student) {
        const all = state.students.all;
        if (!all[number]) {
            Vue.set(all, number, {
                list: [],
                pagination: {
                    page: 0,
                    numPages: -1,
                },
            });
        }

        const item = all[number];
        item.pagination = Object.assign(item.pagination, pagination);

        const { page } = item.pagination;
        Vue.set(all[number].list, page, student);
    },

    [UPDATE_PUBLIC_ELECTIVE_STUDENT_PAGINTION](state, number, pagination) {
        const item = state.students.all[number];
        Object.assign(item.pagination, pagination);
    },

    [UPDATE_PUBLIC_ELECTIVE_STUDENT_CURRENT](state, number, page) {
        const students = state.students;
        const { list, pagination } = students.all[number];
        const _pagination = Object.assign(pagination, { page });
        students.current = {
            list,
            pagination: _pagination,
        };
    },

    [RECEIVE_PUBLIC_ELECTIVE_COMMENT_ITEM](state, number, pagination, comment) {
        const all = state.comments.all;
        if (!all[number]) {
            Vue.set(all, number, {
                list: [],
                pagination: {
                    page: 0,
                    numPages: -1,
                },
            });
        }

        const item = all[number];
        item.pagination = Object.assign(item.pagination, pagination);

        const { page } = item.pagination;
        Vue.set(all[number].list, page, comment);
    },

    [UPDATE_PUBLIC_ELECTIVE_COMMENT_PAGINTION](state, number, pagination) {
        const item = state.comments.all[number];
        Object.assign(item.pagination, pagination);
    },

    [UPDATE_PUBLIC_ELECTIVE_COMMENT_CURRENT](state, number, page) {
        const comments = state.comments;
        const { list, pagination } = comments.all[number];
        const _pagination = Object.assign(pagination, { page });
        comments.current = {
            list,
            pagination: _pagination,
        };
    },

    [RECEIVE_PUBLIC_ELECTIVE_LEADER_BOARD](state, leaderBoard) {
        state.leaderBoard = leaderBoard;
    },

    [RECEIVE_PUBLIC_ELECTIVE_POPULAR](state, popular) {
        state.popular = popular;
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
