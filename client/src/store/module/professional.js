import Vue from "vue";

import {
    UPDATE_PROFESSIONAL_CURRENT,
    RECEIVE_PROFESSIONAL_ITEM,
} from "../mutation-type";

// initial state
const state = {
    pagination: {
        page: 1,
        numPages: 0,
    },
    params: {
        academy: null,
    },
    current: [],
    all: {},
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [UPDATE_PROFESSIONAL_CURRENT](state, { params, pagination }, courses) {
        if (!!params) {
            state.current = [];
            state.params = Object.assign(
                {},
                state.params,
                params
            );
        }

        const { page } = state.pagination = Object.assign(
            {},
            state.pagination,
            pagination
        );

        if (!!courses) {
            Vue.set(state.current, page, courses);
        }
    },

    [RECEIVE_PROFESSIONAL_ITEM](state, number, course) {
        Vue.set(state.all, number, course);
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
