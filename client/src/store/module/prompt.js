import {
    RECEIVE_PROMPT,
    DELETE_PROMPT_ITEM,
} from "../mutation-type";

// initial state
const state = {
    list: null,
};

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
    [RECEIVE_PROMPT](state, list) {
        state.list = list;
    },

    [DELETE_PROMPT_ITEM](state, id) {
        const { list } = state;
        if (!list) {
            state.list = list.filter(
                item => (item && (item.id !== id))
            );
        }
    },
};
/* eslint-enable no-param-reassign */

export default {
    state,
    mutations,
};
