var config = require("../config");
var webpack = require("webpack");
var merge = require("webpack-merge");
var utils = require("./utils");
var baseWebpackConfig = require("./webpack.base.conf");
var HtmlWebpackPlugin = require("html-webpack-plugin");

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
    baseWebpackConfig.entry[name] = [ "./build/dev-client" ].concat(baseWebpackConfig.entry[name]);
});

module.exports = merge(baseWebpackConfig, {
    module: {
        loaders: utils.styleLoaders(),
    },
    // eval-source-map is faster for development
    devtool: "#eval-source-map",
    plugins: [
        new webpack.DefinePlugin({
            "process.env": config.dev.env,
        }),
        // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        // https://github.com/ampedandwired/html-webpack-plugin
        new HtmlWebpackPlugin({
            filename: "pc.html",
            template: "src/index.ejs",
            inject: true,
            chunks: [ "pc" ],
        }),
        new HtmlWebpackPlugin({
            filename: "webapp.html",
            template: "src/index.ejs",
            inject: true,
            webapp: true,
            chunks: [ "webapp" ],
        }),
        new HtmlWebpackPlugin({
            filename: "error.html",
            template: "src/index.ejs",
            inject: true,
            chunks: [ "error" ],
        }),
    ],
});
