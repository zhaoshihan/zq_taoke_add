# -*- coding: utf-8 -*-
import json
import time
import hmac
import base64
import hashlib
import urllib
import requests
import uuid

from aliyun.base import AliyunCommon


class MessageSender(AliyunCommon):
    Action = 'SingleSendSms'
    SignName = '自强淘课啦'
    TemplateCode = ''

    def send_public_elective_season_end_notice(self, student_name, phone_number):
        self.TemplateCode = 'SMS_52275226'
        custom_params = {
            'name': student_name,
        }
        params = {
            'Action': self.Action,
            'SignName': self.SignName,
            'TemplateCode': self.TemplateCode,
            'RecNum': str(phone_number),
            # 这里不用json.dumps就会出现字符串前出现u的前缀，导致验证不通过
            'ParamString': json.dumps(custom_params),
            'Version': '2016-09-27',
            'AccessKeyId': self.ACCESS_KEY_ID,
            'SignatureMethod': 'HMAC-SHA1',
            'Timestamp': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'SignatureVersion': '1.0',
            'SignatureNonce': str(uuid.uuid1()),
        }
        params['Signature'] = self.get_signature(params)
        response = requests.get('https://sms.aliyuncs.com/', params)
        print response.status_code

    def send_abstract_success_message(self, student_name, course_name, phone_number):
        self.TemplateCode = 'SMS_48825039'
        custom_params = {
            'name': student_name,
            'courseName': course_name,
        }
        params = {
            'Action': self.Action,
            'SignName': self.SignName,
            'TemplateCode': self.TemplateCode,
            'RecNum': str(phone_number),
            # 这里不用json.dumps就会出现字符串前出现u的前缀，导致验证不通过
            'ParamString': json.dumps(custom_params),
            'Version': '2016-09-27',
            'AccessKeyId': self.ACCESS_KEY_ID,
            'SignatureMethod': 'HMAC-SHA1',
            'Timestamp': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'SignatureVersion': '1.0',
            'SignatureNonce': str(uuid.uuid1()),
        }
        params['Signature'] = self.get_signature(params)
        response = requests.get('https://sms.aliyuncs.com/', params)
        print response.status_code

    def send_success_message(self, student_number, student_name, course_number, course_name, phone_number):
        self.TemplateCode = 'SMS_43250006'
        custom_params = {
            'studentNumber': str(student_number),
            'studentName': student_name,
            'courseNumber': str(course_number),
            'courseName': course_name,
        }
        params = {
            'Action': self.Action,
            'SignName': self.SignName,
            'TemplateCode': self.TemplateCode,
            'RecNum': str(phone_number),
            # 这里不用json.dumps就会出现字符串前出现u的前缀，导致验证不通过
            'ParamString': json.dumps(custom_params),
            'Version': '2016-09-27',
            'AccessKeyId': self.ACCESS_KEY_ID,
            'SignatureMethod': 'HMAC-SHA1',
            'Timestamp': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'SignatureVersion': '1.0',
            'SignatureNonce': str(uuid.uuid1()),
        }
        params['Signature'] = self.get_signature(params)
        response = requests.get('https://sms.aliyuncs.com/', params)
        print response.status_code

    @staticmethod
    def percent_encode(target_str):
        target_str = str(target_str)
        res = urllib.quote(target_str, '')\
            .replace('+', '%20').replace('*', '%2A').replace('%7E', '~')
        return res

    def get_signature(self, params):
        sorted_params = sorted(params.items(), key=lambda parameters: parameters[0])
        formed_query_string = ''
        for param in sorted_params:
            formed_query_string += '&' + self.percent_encode(param[0]) + '=' + self.percent_encode(param[1])
        string_to_sign = 'GET&%2F&' + self.percent_encode(formed_query_string[1:])
        h = hmac.new(self.ACCESS_KRY_SECRET + '&', string_to_sign, hashlib.sha1)
        signature = base64.encodestring(h.digest()).strip()
        return signature


# if __name__ == '__main__':
#     ms = MessageSender()
#     ms.send_success_message(2015302580161, u'朱奕安', 2333333, u'从不点名没有作业90+的课', 15071295301)
