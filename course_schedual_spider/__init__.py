from spider import CourseScheduleSpider
from database_importer import DataBaseImporter


class CourseScheduleAPI(object):
    __spider = CourseScheduleSpider()
    __database_importer = DataBaseImporter()

    @staticmethod
    def import_new_public_elective_course():
        CourseScheduleAPI.__spider.auto_get_public_elective_course_data()
        CourseScheduleAPI.__database_importer.import_public_elective_course()
        return 'success'

    @staticmethod
    def refresh_public_elective_course_total_left_count():
        CourseScheduleAPI.__spider.auto_get_public_elective_course_data()
        CourseScheduleAPI.__database_importer.refresh_public_elective_course_left_number()
        return 'success'

    @staticmethod
    def adjust_is_open():
        CourseScheduleAPI.__spider.auto_get_public_elective_course_data()
        CourseScheduleAPI.__database_importer.adjust_is_open()

