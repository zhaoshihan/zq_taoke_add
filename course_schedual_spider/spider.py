# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from PIL import Image
from pytesseract import *
import re
import hashlib
import json
import string


class CourseScheduleSpider(object):
    number = '2015302580161'
    password = '2015302580161'
    page_number = 0
    server = 'http://210.42.121.241'
    captcha_url = server + '/servlet/GenImg'
    login_url = server + '/servlet/Login'
    login_success_url = server + '/servlet/../stu/stu_index.jsp'
    first_page_url = server + '/stu/choose_PubLsn_list.jsp?XiaoQu=0&credit=0&keyword=&pageNum=1'
    data_base_url = server + '/stu/choose_PubLsn_list.jsp?XiaoQu=0&credit=0&keyword=&pageNum=%d'

    current_session = requests.session()

    @staticmethod
    def pre_process_image(im):
        for j in range(im.size[0]):
            for i in range(im.size[1]):
                r = [0, 0, 0]
                r[0], r[1], r[2] = im.getpixel((j, i))
                if r[0] + r[1] + r[2] > 400:
                    im.putpixel((j, i), (255, 255, 255))
                elif r[0] < r[1] + r[2]:
                    im.putpixel((j, i), (255, 255, 255))
                else:
                    im.putpixel((j, i), (0, 0, 0))
        for j in range(1, im.size[0] - 1):
            for i in range(1, im.size[1] - 1):
                if im.getpixel((j, i)) == (0, 0, 0) and \
                                im.getpixel((j - 1, i)) == (255, 255, 255) and \
                                im.getpixel((j + 1, i)) == (255, 255, 255) and \
                                im.getpixel((j, i - 1)) == (255, 255, 255) and \
                                im.getpixel((j, i + 1)) == (255, 255, 255):
                    im.putpixel((j, i), (255, 255, 255))
        return im

    def auto_get_captcha_text(self):
        text = ''
        while not text:
            with open('captcha.png', 'wb') as f:
                f.write(self.current_session.get(self.captcha_url).content)
            im = Image.open('captcha.png')
            im = self.pre_process_image(im)
            text = image_to_string(im, lang='captcha', config='-psm 7 captcha')
            if not re.match(r'^[\w\d]{4}$', text):
                text = ''
            print 'recognizing captcha'
        print 'get captcha:' + text
        return text

    def manual_get_captcha_text(self):
        text = ''
        while not text:
            with open('captcha.png', 'wb') as f:
                f.write(self.current_session.get(self.captcha_url).content)
            text = raw_input("请输入验证码，直接回车重新获取验证码：")
        return text

    def get_password_hash(self):
        m = hashlib.md5()
        m.update(self.password)
        return m.hexdigest()

    def auto_login(self):
        while True:
            params = {
                'id': self.number,
                'pwd': self.get_password_hash(),
                'xdvfb': self.auto_get_captcha_text()
            }
            response = self.current_session.get(self.login_url, params=params)
            if response.url == self.login_success_url:
                print 'auto login success'
                return
            else:
                print 'retry login'

    def manual_login(self):
        while True:
            params = {
                'id': self.number,
                'pwd': self.get_password_hash(),
                'xdvfb': self.manual_get_captcha_text()
            }
            response = self.current_session.get(self.login_url, params=params)
            if response.url == self.login_success_url:
                print 'auto login success'
                return

    def get_total_page(self):
        soup = BeautifulSoup(self.current_session.get(self.first_page_url).content, 'lxml')
        count_line = soup.find('div', class_='total_count').text
        m = re.search(u'第1/(\d{2})页', count_line)
        return int(m.groups()[0])

    def get_public_elective_course_data(self):
        data = []
        total_page = int(self.get_total_page())
        for i in range(1, total_page + 1):
            url = self.data_base_url % i
            response = self.current_session.get(url)
            res = self.get_data(response.content.decode('gbk').encode('utf-8'))
            if res:
                data.extend(res)
            print i
        json.dump(data, open('public_elective_course_data.json', 'w'))

    # u'周五:3-14周,每1周; 11-13节,2区,4-205;'
    re_list = [re.compile(u'周([\u4e00\u4e8c\u4e09\u56db\u4e94\u516d\u4e03\u65e51234567])'),
               re.compile(u'(\d+)-(\d+)周'),
               re.compile(u'每(\d+)周'),
               re.compile(u'(\d+)-(\d+)节'),
               re.compile(u'(.+)'), ]
    weekday_map = {
        u'一': 1,
        u'二': 2,
        u'三': 3,
        u'四': 4,
        u'五': 5,
        u'六': 6,
        u'七': 7,
        u'日': 7,

    }

    def filter_week_day(self, raw_weekday):
        if raw_weekday in self.weekday_map:
            return self.weekday_map[raw_weekday]
        else:
            # 本身即为数字或者为空
            return raw_weekday

    @staticmethod
    def get_time_from_re(m, group_index):
        if m:
            return m.group(group_index).strip(string.punctuation)
        else:
            return None

    def get_course_time_location(self, time_text):
        match_list = []
        for r in self.re_list:
            m = re.search(r, time_text)
            match_list.append(m)
            if m:
                time_text = time_text[m.end():]
        location = self.get_time_from_re(match_list[4], 1)
        time = {
            'weekday': self.filter_week_day(self.get_time_from_re(match_list[0], 1)),
            'week_from': self.get_time_from_re(match_list[1], 1),
            'week_to': self.get_time_from_re(match_list[1], 2),
            'repeat': self.get_time_from_re(match_list[2], 1),
            'class_begin': self.get_time_from_re(match_list[3], 1),
            'class_over': self.get_time_from_re(match_list[3], 2),
            'location': location,
        }
        return time, location

    def get_data(self, html_text):
        data = []
        soup = BeautifulSoup(html_text.decode('utf-8'), 'html5lib')
        for tr in soup.find_all('tr')[1:]:
            tds = tr.find_all('td')
            time_text = tds[9].div.text.strip().replace('\n', '').replace('\t', '').replace(' ', '')
            time, location = self.get_course_time_location(time_text)
            col = {
                'name': tds[0].text,
                'credit': tds[1].text,
                'left': tds[2].text.split('/')[0],
                'total': tds[2].text.split('/')[1],
                'teacher': tds[3].text,
                'title': tds[4].text,
                'academy': tds[5].text,
                'book': tds[6].text,
                'year': tds[7].text,
                'semester': tds[8].text,
                'time': [time],
                'location': location,
                'field': tds[10].text.strip(),
                'number': tds[11].input['id'],
            }
            data.append(col)
        return data

    def auto_get_public_elective_course_data(self):
        self.auto_login()
        self.get_public_elective_course_data()

    def manual_get_public_elective_course_data(self):
        self.manual_login()
        self.get_public_elective_course_data()
