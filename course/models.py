# -*- coding: utf-8 -*-
"""
course model方法说明

course model方法较为简单，没有与api的交互，基本都是数据库的查询

几个复杂的函数容易出错的都是内部调用的

返回就是该返回什么就返回什么

每个model方法必须写注释！！！
"""
from functools import wraps
import logging
import json

from datetime import datetime

from django.db import models
from django.db.models import Sum
from django.core.cache import cache

from zq_taoke_rebuild import settings


logger = logging.getLogger('database_info')

# Create your models here.


def static_data_cache(method):
    # 此wraps装饰器是快速传递函数的元数据的
    @wraps(method)
    def wrapper(force=None):
        cache_key = method.func_name
        if not force:
            data = cache.get(cache_key, None)
            if data:
                return data
        # 如果是force或者是取缓存为空则进行查询
        data = method()
        cache.set(cache_key, data, None)
        return data

    return wrapper


class Department(models.Model):
    name = models.CharField(u'学部', max_length=10)
    num = models.IntegerField(verbose_name=u'学院编号', default=0)
    # {
    #     u'文理学部': 1,
    #     u'工学部': 2,
    #     u'信息学部': 3,
    #     u'医学部': 4,
    # }

    class Meta:
        verbose_name = u'学部类'
        verbose_name_plural = u'学部类'

    def __unicode__(self):
        return self.name

    @staticmethod
    @static_data_cache
    def get_department_list():
        department_list = []
        for department in Department.objects.all():
            department_list.append(department.name)
        return department_list


class Academy(models.Model):
    name = models.CharField(verbose_name=u'所属学院', max_length=20, db_index=True, blank=True, null=True)
    department = models.ForeignKey(verbose_name=u'所属学部', to=Department, blank=True, null=True)

    class Meta:
        verbose_name = u'学院'
        verbose_name_plural = u'学院'

    def __unicode__(self):
        return self.name

    @staticmethod
    @static_data_cache
    def get_department_and_academy_list():
        department_and_academy_list = []
        # 这里是Department对Academy指向它的ForeignKey的反向查询
        # 是一对多的关系, 所以不能使用select_related(), 只能用prefetch_related()
        # prefetch_related()执行过程分两部走
        # 先获取Department的查询结果
        # 再查询Academy内外键key IN Department的结果的id集合中
        # 避免了很多零散查询
        # 但是要注意in查询的效率
        # https://docs.djangoproject.com/en/1.8/ref/models/querysets/#django.db.models.query.QuerySet.prefetch_related
        for department in Department.objects.all().prefetch_related('academy_set'):
            sub_data = {
                'department': department.name,
                'academies': [],
            }
            for academy in department.academy_set.all():
                sub_data['academies'].append(academy.name)
            department_and_academy_list.append(sub_data)
        return department_and_academy_list


class Teacher(models.Model):
    name = models.CharField(verbose_name=u'教师名字', max_length=20, blank=True, null=True)
    title = models.CharField(verbose_name=u'职称', max_length=20, blank=True, null=True)

    class Meta:
        verbose_name = u'教师'
        verbose_name_plural = u'教师'

    def __unicode__(self):
        return u'%s教师' % self.name

    def get_this_teacher_all_comment(self, target_teacher_id):
        comment = []
        this_teacher_course_set = PublicElectiveCourseSet.objects.filter(teacher_id=target_teacher_id)
        for course in this_teacher_course_set:
            course_comment_set = course.comment_set.exclude(content='')
            for one_comment in course_comment_set:
                comment.extend(one_comment.content)
        return comment


    def get_this_teacher_all_score(self, target_teacher_id):
        PublicElectiveCourseSet.refresh_giving_score_statistics()
        this_teacher_course_set = PublicElectiveCourseSet.objects.filter(teacher_id=target_teacher_id)

        attendance = this_teacher_course_set.aggregate(Sum('check_attendance_good'), Sum('check_attendance_medium'),
                                                       Sum('check_attendance_bad'))
        quality = this_teacher_course_set.aggregate(Sum('teaching_quality_good'), Sum('teaching_quality_medium'),
                                                    Sum('teaching_quality_bad'))
        score = this_teacher_course_set.aggregate(Sum('giving_score_good'), Sum('giving_score_medium'),
                                                  Sum('giving_score_bad'))

        return attendance, quality, score


class CourseSetBase(models.Model):
    name = models.CharField(verbose_name=u'课程名称', db_index=True, max_length=30, blank=True, null=True)
    teacher = models.ForeignKey(verbose_name=u'授课教师', to=Teacher, blank=True, null=True)
    academy = models.ForeignKey(verbose_name=u'所属学院', to=Academy, blank=True, null=True)
    summary = models.TextField(verbose_name=u'课程简介', default=u'', blank=True, null=True)
    credit = models.FloatField(verbose_name=u'学分', default=0, blank=True, null=True)
    # 记得更新这个字段啊
    is_open = models.BooleanField(verbose_name=u'本学期是否有课程开课', default=False)

    class Meta:
        abstract = True
        verbose_name = u'同课程名同教师的课程集合基类'
        verbose_name_plural = u'同课程名同教师的课程集合基类'


class PublicElectiveCourseSet(CourseSetBase):
    field = models.CharField(verbose_name=u'领域', max_length=20, blank=True, null=True)
    # 所有default为1是放置被0除，并且稀释评价较少的时候的极端对比
    check_attendance_good = models.IntegerField(verbose_name=u'考勤力度毫无压力', default=1)
    check_attendance_medium = models.IntegerField(verbose_name=u'考勤力度偶尔点点', default=1)
    check_attendance_bad = models.IntegerField(verbose_name=u'考勤力度逢课必点', default=1)
    teaching_quality_good = models.IntegerField(verbose_name=u'授课质量引人入胜', default=1)
    teaching_quality_medium = models.IntegerField(verbose_name=u'授课质量中规中矩', default=1)
    teaching_quality_bad = models.IntegerField(verbose_name=u'授课质量照本宣科', default=1)
    giving_score_good = models.IntegerField(verbose_name=u'给分90+', default=1)
    giving_score_medium = models.IntegerField(verbose_name=u'给分80-90', default=1)
    giving_score_bad = models.IntegerField(verbose_name=u'给分80-', default=1)
    is_popular = models.BooleanField(verbose_name=u'是否为热门课程', default=False)
    is_check_attendance_top_ten = models.BooleanField(verbose_name=u'是否为考勤前十', default=False)
    is_teaching_quality_top_ten = models.BooleanField(verbose_name=u'是否为授课前十', default=False)
    is_giving_score_top_ten = models.BooleanField(verbose_name=u'是否为给分前十', default=False)
    is_boy_top_ten = models.BooleanField(verbose_name=u'是否为男生前十', default=False)
    is_girl_top_ten = models.BooleanField(verbose_name=u'是否为女生前十', default=False)

    # 本学期的课程集 可以通过 (self_instance).courses.filter(is_open==True) 获得
    # 本课程的所有评价 可以通过 (self_instance).comment_set.all()获得

    class Meta:
        verbose_name = u'公选课程集合'
        verbose_name_plural = u'公选课程集合'

    def __unicode__(self):
        return u'%s的%s' % (self.teacher, self.name)

    def get_course_set_detail(self):
        data = {
            'course_set': {
                'name': self.name,
                'teacher': self.teacher.name,
                'academy': self.academy.name,
                'summary': self.summary,
                'credit': self.credit,
                'is_open': self.is_open,
                'field': self.field,
                'check_attendance_good': self.check_attendance_good,
                'check_attendance_medium': self.check_attendance_medium,
                'check_attendance_bad': self.check_attendance_bad,
                'teaching_quality_good': self.teaching_quality_good,
                'teaching_quality_medium': self.teaching_quality_medium,
                'teaching_quality_bad': self.teaching_quality_bad,
                'giving_score_good': self.giving_score_good,
                'giving_score_medium': self.giving_score_medium,
                'giving_score_bad': self.giving_score_bad,
                'is_popular': self.is_popular,
                'is_check_attendance_top_ten': self.is_check_attendance_top_ten,
                'is_teaching_quality_top_ten': self.is_teaching_quality_top_ten,
                'is_giving_score_top_ten': self.is_giving_score_top_ten,
                'is_boy_top_ten': self.is_boy_top_ten,
                'is_girl_top_ten': self.is_girl_top_ten,
            },
            'courses': self.get_all_courses_this_semester(),
        }
        return data

    def get_one_course_and_other_courses_this_semester_in_this_set(self):
        courses = self.courses.filter(is_open=True)
        if courses:
            data = courses[0].get_course_detail()
            data['other_courses'] = []
            for course in courses[1:]:
                data['other_courses'].append(course.get_course_detail())
            return data
        else:
            if self.is_open:
                self.is_open = False
                self.save()
            logger.error('course %s which is not open is being visited' % self)
            return {}

    def get_all_courses_this_semester(self):
        """
        :return: 课程详情的列表
        """
        query_set = self.courses.filter(is_open=True)
        data = PublicElectiveCourse.get_all_detail(query_set)
        return data

    def get_all_comment(self, page=1, per_page=-1):
        """
        :return: 评价详情的列表
        """
        data = []
        all_comment = self.comment_set.exclude(content='').select_related('student')\
            .only('content', 'is_anonymous', 'publish_time', 'course_set',
                  'check_attendance_valuation', 'teaching_quality_valuation', 'giving_score_valuation',
                  'student__name', 'student__avatar_type_switch', 'student__avatar', 'student__sys_avatar')
        if per_page == -1:
            pagination = {
                'page': 1,
                'num_pages': 1,
            }
            for comment in all_comment:
                data.append(comment.get_comment_detail())
        else:
            all_comment_count = all_comment.count()
            total_page = all_comment_count / per_page
            if all_comment_count % per_page:
                total_page += 1
            if not total_page:  # 排除总数为0的切片错误
                total_page = 1
            if page > total_page:
                page = total_page
            pagination = {
                'page': page,
                'num_pages': total_page,
            }
            comment_page = all_comment[per_page * (page - 1):per_page * page]
            for comment in comment_page:
                data.append(comment.get_comment_detail())
        return data, pagination

    @staticmethod
    def refresh_giving_score_statistics():
        for course_set in PublicElectiveCourseSet.objects.all():
            total_count = course_set.publicelectivestudentcourse_set.all().count()
            good_count = course_set.publicelectivestudentcourse_set.filter(score__gte=90).count()
            bad_count = course_set.publicelectivestudentcourse_set.filter(score__lt=80).count()
            # 筛去未给分的
            not_scored_count = course_set.publicelectivestudentcourse_set.filter(score__lt=0).count()
            course_set.giving_score_good = good_count
            course_set.giving_score_medium = total_count - good_count - bad_count
            course_set.giving_score_bad = bad_count - not_scored_count
            if not course_set.giving_score_good:
                course_set.giving_score_good = 1
            if not course_set.giving_score_medium:
                course_set.giving_score_medium = 1
            if not course_set.giving_score_bad:
                course_set.giving_score_bad = 1
            course_set.save()

    @staticmethod
    def get_top_ten(force=None):
        """
        获取考勤授课给分前十的课程集合
        用户访问正常情况下应该是not force模式，直接从缓存中取
        force刷新请加入定时任务，并且注意耗时较长
        :param force: 是否强制更新
        :return: dic: 'top_ten_type':course_set instance list / string: message, bool: success
        """
        if not force:
            # todo:是不是可以直接filter查询？
            data = cache.get('top_ten_course_set', None)
            if data:
                return data, True
            else:
                # 用户访问为not force，但是没有取到缓存，直接返回空，打critical日志
                # 用户是没有耐心来等服务器重新跑一遍数据库拿到新鲜的数据的
                # todo：使用异步任务刷新， 使用异步刷新了之后就可以使用static_data_cache装饰器，启动异步任务刷新缓存并先行返回None

                logger.critical(u'top_ten_course_set cache fetch failed, plz refresh')
                return 11, False
        else:
            # 遍历数据库刷新排行榜
            # 如果没有正常执行完成打critical日志
            # 先刷新一下给分情况
            PublicElectiveCourseSet.refresh_giving_score_statistics()
            try:
                # 先把所有top_ten数据清空
                for course_set in PublicElectiveCourseSet.objects.filter(models.Q(is_check_attendance_top_ten=True) |
                                                                         models.Q(is_check_attendance_top_ten=True) |
                                                                         models.Q(is_giving_score_top_ten=True) |
                                                                         models.Q(is_boy_top_ten=True) |
                                                                         models.Q(is_girl_top_ten=True)):
                    course_set.is_check_attendance_top_ten = False
                    course_set.is_teaching_quality_top_ten = False
                    course_set.is_giving_score_top_ten = False
                    course_set.is_boy_top_ten = False
                    course_set.is_girl_top_ten = False
                    course_set.save()
                print 'data clearance complete'
                # 再开始计算
                check_attendance_top_ten = []
                teaching_quality_top_ten = []
                giving_score_top_ten = []
                boy_top_ten = []
                girl_top_ten = []

                for course_set in PublicElectiveCourseSet.objects.filter(is_open=True):
                    total_comment = course_set.comment_set.all().count()
                    boy_count = course_set.courses.filter(student_courses__student__gender=u'男').count()
                    girl_count = course_set.courses.filter(student_courses__student__gender=u'女').count()
                    # 根据设置的最小评论数最小男女生数筛选
                    # todo: 这里的评分算法是不是可以优化一下，哦还有根据发布时间进行加权也很重要
                    if total_comment >= settings.TOP_TEN_FILTER['MIN_COMMENT']:
                        can_join_comment_top_ten = True
                    else:
                        can_join_comment_top_ten = False
                    if boy_count >= settings.TOP_TEN_FILTER['MIN_BOY']:
                        can_join_boy_top_ten = True
                    else:
                        can_join_boy_top_ten = False
                    if girl_count >= settings.TOP_TEN_FILTER['MIN_GIRL']:
                        can_join_girl_top_ten = True
                    else:
                        can_join_girl_top_ten = False
                    # 将课程按照评分从高到低依次加入top ten的列表
                    if can_join_comment_top_ten:

                        course_set.check_attendance_valuation = float(course_set.check_attendance_good * 3 +
                                                                      course_set.check_attendance_medium * 2 +
                                                                      course_set.check_attendance_bad * 1) / float(
                            total_comment)
                        if check_attendance_top_ten:
                            # 临时字段，不会存入数据库
                            if len(check_attendance_top_ten) < 10:
                                # 如果前十的排名不为空，并且数量没有到达十个
                                # 从高到低查找本次计算的课程按照评分应该插入到哪个位置并插入
                                i = 0
                                is_inserted = False
                                for high_score_course_set in check_attendance_top_ten:
                                    if high_score_course_set.check_attendance_valuation < \
                                            course_set.check_attendance_valuation:
                                        is_inserted = True
                                        check_attendance_top_ten.insert(i, course_set)
                                        break
                                    i += 1
                                if not is_inserted:
                                    check_attendance_top_ten.append(course_set)
                            else:
                                # 如果前十的排名不为空，并且数量已经到达十个
                                # 从高到低查找本次计算的课程按照评分应该插入到哪个位置并插入
                                # 最后弹出最后一个课程
                                i = 0
                                for high_score_course_set in check_attendance_top_ten:
                                    if high_score_course_set.check_attendance_valuation < \
                                            course_set.check_attendance_valuation:
                                        check_attendance_top_ten.insert(i, course_set)
                                        check_attendance_top_ten.pop()
                                        break
                                    i += 1
                        else:
                            # 如果前十的排名为空，直接加入
                            check_attendance_top_ten.append(course_set)

                        course_set.teaching_quality_valuation = float(course_set.teaching_quality_good * 3 +
                                                                      course_set.teaching_quality_medium * 2 +
                                                                      course_set.teaching_quality_bad * 1) / float(
                            total_comment)
                        if teaching_quality_top_ten:
                            if len(teaching_quality_top_ten) < 10:
                                i = 0
                                is_inserted = False
                                for high_score_course_set in teaching_quality_top_ten:
                                    if high_score_course_set.teaching_quality_valuation < \
                                            course_set.teaching_quality_valuation:
                                        is_inserted = True
                                        teaching_quality_top_ten.insert(i, course_set)
                                        break
                                    i += 1
                                if not is_inserted:
                                    teaching_quality_top_ten.append(course_set)
                            else:
                                i = 0
                                for high_score_course_set in teaching_quality_top_ten:
                                    if high_score_course_set.teaching_quality_valuation < \
                                            course_set.teaching_quality_valuation:
                                        teaching_quality_top_ten.insert(i, course_set)
                                        teaching_quality_top_ten.pop()
                                        break
                                    i += 1
                        else:
                            teaching_quality_top_ten.append(course_set)

                        course_set.giving_score_valuation = float(course_set.giving_score_good * 3 +
                                                                  course_set.giving_score_medium * 2 +
                                                                  course_set.giving_score_bad * 1) / float(
                            total_comment)
                        if giving_score_top_ten:
                            if len(giving_score_top_ten) < 10:
                                i = 0
                                is_inserted = False
                                for high_score_course_set in giving_score_top_ten:
                                    if high_score_course_set.giving_score_valuation < course_set.giving_score_valuation:
                                        is_inserted = True
                                        giving_score_top_ten.insert(i, course_set)
                                        break
                                    i += 1
                                if not is_inserted:
                                    giving_score_top_ten.append(course_set)
                            else:
                                i = 0
                                for high_score_course_set in giving_score_top_ten:
                                    if high_score_course_set.giving_score_valuation < course_set.giving_score_valuation:
                                        giving_score_top_ten.insert(i, course_set)
                                        giving_score_top_ten.pop()
                                        break
                                    i += 1
                        else:
                            giving_score_top_ten.append(course_set)

                    if can_join_boy_top_ten:
                        course_set.boy_percentage = float(boy_count) / float(boy_count + girl_count)
                        if boy_top_ten:
                            if len(boy_top_ten) < 10:
                                i = 0
                                is_inserted = False
                                for high_score_course_set in boy_top_ten:
                                    if high_score_course_set.boy_percentage < \
                                            course_set.boy_percentage:
                                        is_inserted = True
                                        boy_top_ten.insert(i, course_set)
                                        break
                                    i += 1
                                if not is_inserted:
                                    boy_top_ten.append(course_set)
                            else:
                                i = 0
                                for high_score_course_set in boy_top_ten:
                                    if high_score_course_set.boy_percentage < \
                                            course_set.boy_percentage:
                                        boy_top_ten.insert(i, course_set)
                                        boy_top_ten.pop()
                                        break
                                    i += 1
                        else:
                            boy_top_ten.append(course_set)

                    if can_join_girl_top_ten:
                        course_set.girl_percentage = float(girl_count) / float(boy_count + girl_count)
                        if girl_top_ten:
                            if len(girl_top_ten) < 10:
                                i = 0
                                is_inserted = False
                                for high_score_course_set in girl_top_ten:
                                    if high_score_course_set.girl_percentage < \
                                            course_set.girl_percentage:
                                        is_inserted = True
                                        girl_top_ten.insert(i, course_set)
                                        break
                                    i += 1
                                if not is_inserted:
                                    girl_top_ten.append(course_set)
                            else:
                                i = 0
                                for high_score_course_set in girl_top_ten:
                                    if high_score_course_set.girl_percentage < \
                                            course_set.girl_percentage:
                                        girl_top_ten.insert(i, course_set)
                                        girl_top_ten.pop()
                                        break
                                    i += 1
                        else:
                            girl_top_ten.append(course_set)

                print 'calculation complete'
                # 赋值
                check_attendance_top_ten_detail = []
                teaching_quality_top_ten_detail = []
                giving_score_top_ten_detail = []
                boy_top_ten_detail = []
                girl_top_ten_detail = []
                for course_set in check_attendance_top_ten:
                    course_set.is_check_attendance_top_ten = True
                    course_set.save()
                    check_attendance_top_ten_detail.append(course_set.
                                                           get_one_course_and_other_courses_this_semester_in_this_set())
                for course_set in teaching_quality_top_ten:
                    course_set.is_teaching_quality_top_ten = True
                    course_set.save()
                    teaching_quality_top_ten_detail.append(course_set.
                                                           get_one_course_and_other_courses_this_semester_in_this_set())
                for course_set in giving_score_top_ten:
                    course_set.is_giving_score_top_ten = True
                    course_set.save()
                    giving_score_top_ten_detail.append(course_set.
                                                       get_one_course_and_other_courses_this_semester_in_this_set())
                for course_set in boy_top_ten:
                    course_set.is_boy_top_ten = True
                    course_set.save()
                    boy_top_ten_detail.append(course_set.
                                              get_one_course_and_other_courses_this_semester_in_this_set())
                for course_set in girl_top_ten:
                    course_set.is_girl_top_ten = True
                    course_set.save()
                    girl_top_ten_detail.append(course_set.
                                               get_one_course_and_other_courses_this_semester_in_this_set())
                data = {
                    'attendance': check_attendance_top_ten_detail,
                    'quality': teaching_quality_top_ten_detail,
                    'score': giving_score_top_ten_detail,
                    'boy': boy_top_ten_detail,
                    'girl': girl_top_ten_detail,
                }
                # 存入缓存
                cache.set('top_ten_course_set', data, None)
                return data, True
            except Exception, error:
                logger.critical(u'%s, failed to refresh top ten course set, please debug and refresh again' % error)
                return '%s, failed to refresh top ten course set, please debug and refresh again' % error, False


class SpecializedCourseSet(CourseSetBase):
    TYPE_CHOICE = {
        (u'专业必修', u'专业必修'),
        (u'专业选修', u'专业选修'),
    }
    type = models.CharField(verbose_name=u'课程类型', max_length=5, choices=TYPE_CHOICE, blank=True, null=True)
    major = models.CharField(verbose_name=u'专业名称', max_length=30, blank=True, null=True)
    recommend_count = models.IntegerField(verbose_name=u'专业课推荐次数', default=0, blank=True, null=True)
    star = models.IntegerField(verbose_name=u'推荐星级', default=0)
    update_date = models.DateField(verbose_name=u'推荐星级更新日期', auto_now_add=True)

    # 本学期的课程集 可以通过 (self_instance).courses.filter(is_open==True) 获得

    class Meta:
        verbose_name = u'专业课程集合'
        verbose_name_plural = u'公选课课程集合'
        ordering = ['-star']

    # Student.recommend_specialized_course() 推荐专业课
    # Student.delete_recommended_specialized_course() 删除推荐专业课

    def get_course_set_detail(self):
        data = {
            'course_set': {
                'name': self.name,
                'teacher': self.teacher.name,
                'academy': self.academy.name,
                'summary': self.summary,
                'credit': self.credit,
                'is_open': self.is_open,
                'type': self.type,
                'major': self.major,
                'recommend_count': self.recommend_count,
                'star': self.star,
            },
            'courses': self.get_course_detail_this_semester(),
        }
        return data

    def get_course_detail_this_semester(self):
        all_courses = self.courses.filter(is_open=True)
        data = []
        for course in all_courses:
            data.append(course.get_course_detail())
        return data

    @staticmethod
    def calculate_rank():
        all_courses_list = SpecializedCourseSet.objects.filter(is_open=True).order_by('-star')
        total_count = SpecializedCourseSet.objects.filter(is_open=True).count()
        for course in all_courses_list[int(total_count * settings.SPECIALIZED_COURSE_STAR['ONE_STAR'][0]):
                                       int(total_count * settings.SPECIALIZED_COURSE_STAR['ONE_STAR'][1])]:
            course.star = 1
            course.date = datetime.now()
            course.save()
        for course in all_courses_list[int(total_count * settings.SPECIALIZED_COURSE_STAR['TWO_STAR'][0]):
                                       int(total_count * settings.SPECIALIZED_COURSE_STAR['TWO_STAR'][1])]:
            course.star = 2
            course.date = datetime.now()
            course.save()
        for course in all_courses_list[int(total_count * settings.SPECIALIZED_COURSE_STAR['THREE_STAR'][0]):
                                       int(total_count * settings.SPECIALIZED_COURSE_STAR['THREE_STAR'][1])]:
            course.star = 3
            course.date = datetime.now()
            course.save()
        for course in all_courses_list[int(total_count * settings.SPECIALIZED_COURSE_STAR['FOUR_STAR'][0]):
                                       int(total_count * settings.SPECIALIZED_COURSE_STAR['FOUR_STAR'][1])]:
            course.star = 4
            course.date = datetime.now()
            course.save()
        for course in all_courses_list[int(total_count * settings.SPECIALIZED_COURSE_STAR['FIVE_STAR'][0]):
                                       int(total_count * settings.SPECIALIZED_COURSE_STAR['FIVE_STAR'][1])]:
            course.star = 5
            course.date = datetime.now()
            course.save()


class PublicCompulsoryCourseSet(CourseSetBase):

    class Meta:
        verbose_name = u'公必课程集合'
        verbose_name_plural = u'公必课课程集合'


class CourseBase(models.Model):
    number = models.BigIntegerField(verbose_name=u'课头号', db_index=True, blank=True, null=True)
    name = models.CharField(verbose_name=u'课程名', max_length=30, blank=True, null=True)
    book = models.CharField(verbose_name=u'所用教材', default=u'', max_length=50, blank=True, null=True)
    teacher = models.ForeignKey(verbose_name=u'授课教师', to=Teacher, blank=True, null=True)
    academy = models.ForeignKey(verbose_name=u'所属学院', to=Academy, blank=True, null=True)
    week_from = models.IntegerField(verbose_name=u'在x周开课', default=0, blank=True, null=True)
    week_to = models.IntegerField(verbose_name=u'在x周结课', default=0, blank=True, null=True)
    credit = models.FloatField(verbose_name=u'学分', default=0, blank=True, null=True)
    is_open = models.BooleanField(verbose_name=u'本学期是否开课', default=False)

    class Meta:
        abstract = True
        verbose_name = u'课程基类'
        verbose_name_plural = u'课程基类'

    def __unicode__(self):
        return u'%s%s' % (self.number, self.name)

    @staticmethod
    def get_or_create_course(data):
        """
        此方法暂时是在用户获取课表的时候添加新课程
        :param data: formed data from the API
        :return: Course object created
        """
        semester_id = str(settings.SEMESTER_DATE['YEAR']) + str(settings.SEMESTER_DATE['SEMESTER'])
        if str(data['identifier']).startswith(semester_id):
            is_open = True
        else:
            is_open = False
        if data['course_type'] == u'公共选修':
            try:
                course = PublicElectiveCourse.objects.get(number=data['identifier'])
                if not course.course_time.all():
                    week_from = 100
                    week_to = 0
                    for course_time in data['lessons_time']:
                        new_course_time = PublicElectiveCourseTime(course=course, location=course_time['location'],
                                                                   class_begin=course_time['class_begin'],
                                                                   class_over=course_time['class_over'],
                                                                   weekday=course_time['weekday'],
                                                                   repeat=course_time['repeats'],
                                                                   week_from=course_time['week_from'],
                                                                   week_to=course_time['week_to'])
                        new_course_time.save()
                        if week_from > course_time['week_from']:
                            week_from = course_time['week_from']
                        if week_to < course_time['week_to']:
                            week_to = course_time['week_to']
                    # 如果有上课时间信息就可以推断出课程的开始结束周
                    if week_from != 100 and week_to:
                        course.week_from = week_from
                        course.week_to = week_to
                        course.save()
                return course
            except PublicElectiveCourse.DoesNotExist:
                # 取出或者创建对应的教师对象
                try:
                    teacher = Teacher.objects.get(name=data['instructor'])
                except Teacher.DoesNotExist:
                    teacher = Teacher(name=data['instructor'])
                    teacher.save()
                # 取出或者创建对应的学院对象
                try:
                    academy = Academy.objects.get(name=data['college'])
                except Academy.DoesNotExist:
                    academy = Academy(name=data['college'])
                    academy.save()
                # 取出或者创建对应的课程集合对象
                try:
                    course_set = PublicElectiveCourseSet.objects.get(name=data['name'], teacher=teacher,
                                                                     academy=academy, credit=data['credit'])
                except PublicElectiveCourseSet.DoesNotExist:
                    course_set = PublicElectiveCourseSet(name=data['name'], teacher=teacher, academy=academy,
                                                         credit=data['credit'])
                    course_set.save()
                # 用判断是否有给分的方式来判断是否开课
                course = PublicElectiveCourse(number=data['identifier'], name=data['name'], course_set=course_set,
                                              book=data.get('book'), teacher=teacher, academy=academy,
                                              credit=data['credit'], is_open=False)
                course.save()
                # 创建上课时间信息
                week_from = 100
                week_to = 0
                for course_time in data['lessons_time']:
                    new_course_time = PublicElectiveCourseTime(course=course, location=course_time['location'],
                                                               class_begin=course_time['class_begin'],
                                                               class_over=course_time['class_over'],
                                                               weekday=course_time['weekday'],
                                                               repeat=course_time['repeats'],
                                                               week_from=course_time['week_from'],
                                                               week_to=course_time['week_to'])
                    new_course_time.save()
                    if week_from > course_time['week_from']:
                        week_from = course_time['week_from']
                    if week_to < course_time['week_to']:
                        week_to = course_time['week_to']
                # 如果有上课时间信息就可以推断出课程的开始结束周
                if week_from != 100 and week_to:
                    course.week_from = week_from
                    course.week_to = week_to
                    course.save()
                return course
        elif data['course_type'] == u'公共必修':
            try:
                course = PublicCompulsoryCourse.objects.get(number=data['identifier'])
                if not course.course_time.all():
                    week_from = 100
                    week_to = 0
                    for course_time in data['lessons_time']:
                        print data['identifier']
                        print course_time
                        new_course_time = PublicCompulsoryCourseTime(course=course, location=course_time['location'],
                                                                     class_begin=course_time['class_begin'],
                                                                     class_over=course_time['class_over'],
                                                                     weekday=course_time['weekday'],
                                                                     repeat=course_time['repeats'],
                                                                     week_from=course_time['week_from'],
                                                                     week_to=course_time['week_to'])
                        new_course_time.save()
                        if week_from > course_time['week_from']:
                            week_from = course_time['week_from']
                        if week_to < course_time['week_to']:
                            week_to = course_time['week_to']
                    # 如果有上课时间信息就可以推断出课程的开始结束周
                    if week_from != 100 and week_to:
                        course.week_from = week_from
                        course.week_to = week_to
                        course.save()
                return course
            except PublicCompulsoryCourse.DoesNotExist:
                # 取出或者创建对应的教师对象
                try:
                    teacher = Teacher.objects.get(name=data['instructor'])
                except Teacher.DoesNotExist:
                    teacher = Teacher(name=data['instructor'])
                    teacher.save()
                # 取出或者创建对应的学院对象
                try:
                    academy = Academy.objects.get(name=data['college'])
                except Academy.DoesNotExist:
                    academy = Academy(name=data['college'])
                    academy.save()
                # 取出或者创建对应的课程集合对象
                try:
                    course_set = PublicCompulsoryCourseSet.objects.get(name=data['name'], teacher=teacher,
                                                                       academy=academy, credit=data['credit'])
                except PublicCompulsoryCourseSet.DoesNotExist:
                    course_set = PublicCompulsoryCourseSet(name=data['name'], teacher=teacher, academy=academy,
                                                           credit=data['credit'])
                    course_set.save()
                course = PublicCompulsoryCourse(number=data['identifier'], name=data['name'], course_set=course_set,
                                                book=data.get('book'), teacher=teacher, academy=academy,
                                                credit=data['credit'], is_open=is_open)
                course.save()
                # 创建上课时间信息
                week_from = 100
                week_to = 0
                for course_time in data['lessons_time']:
                    new_course_time = PublicCompulsoryCourseTime(course=course, location=course_time['location'],
                                                                 class_begin=course_time['class_begin'],
                                                                 class_over=course_time['class_over'],
                                                                 weekday=course_time['weekday'],
                                                                 repeat=course_time['repeats'],
                                                                 week_from=course_time['week_from'],
                                                                 week_to=course_time['week_to'])
                    new_course_time.save()
                    if week_from > course_time['week_from']:
                        week_from = course_time['week_from']
                    if week_to < course_time['week_to']:
                        week_to = course_time['week_to']
                # 如果有上课时间信息就可以推断出课程的开始结束周
                if week_from != 100 and week_to:
                    course.week_from = week_from
                    course.week_to = week_to
                    course.save()
                return course
        elif data['course_type'].startswith(u'专业'):
            try:
                course = SpecializedCourse.objects.get(number=data['identifier'])
                if not course.course_time.all():
                    week_from = 100
                    week_to = 0
                    for course_time in data['lessons_time']:
                        new_course_time = SpecializedCourseTime(course=course, location=course_time['location'],
                                                                class_begin=course_time['class_begin'],
                                                                class_over=course_time['class_over'],
                                                                weekday=course_time['weekday'],
                                                                repeat=course_time['repeats'],
                                                                week_from=course_time['week_from'],
                                                                week_to=course_time['week_to'])
                        new_course_time.save()
                        if week_from > course_time['week_from']:
                            week_from = course_time['week_from']
                        if week_to < course_time['week_to']:
                            week_to = course_time['week_to']
                    # 如果有上课时间信息就可以推断出课程的开始结束周
                    if week_from != 100 and week_to:
                        course.week_from = week_from
                        course.week_to = week_to
                        course.save()
                return course
            except SpecializedCourse.DoesNotExist:
                # 取出或者创建对应的教师对象
                try:
                    teacher = Teacher.objects.get(name=data['instructor'])
                except Teacher.DoesNotExist:
                    teacher = Teacher(name=data['instructor'])
                    teacher.save()
                # 取出或者创建对应的学院对象
                try:
                    academy = Academy.objects.get(name=data['college'])
                except Academy.DoesNotExist:
                    academy = Academy(name=data['college'])
                    academy.save()
                # 用判断是否有给分的方式来判断是否开课
                # if data['grade'] > 0:
                #     is_open = False
                # else:
                #     is_open = True
                # 取出或者创建对应的课程集合对象
                try:
                    course_set = SpecializedCourseSet.objects.get(name=data['name'], teacher=teacher,
                                                                  academy=academy, credit=data['credit'],
                                                                  major=data['major'])
                except SpecializedCourseSet.DoesNotExist:
                    course_set = SpecializedCourseSet(name=data['name'], teacher=teacher, academy=academy,
                                                      credit=data['credit'], major=data['major'], is_open=False)
                    course_set.save()
                # 根据信息创建新课程
                course = SpecializedCourse(number=data['identifier'], name=data['name'], course_set=course_set,
                                           book=data.get('book'), teacher=teacher, academy=academy,
                                           credit=data['credit'], major=data['major'], is_open=False)
                course.save()
                # 创建上课时间信息
                week_from = 100
                week_to = 0
                for course_time in data['lessons_time']:
                    new_course_time = SpecializedCourseTime(course=course, location=course_time['location'],
                                                            class_begin=course_time['class_begin'],
                                                            class_over=course_time['class_over'],
                                                            weekday=course_time['weekday'],
                                                            repeat=course_time['repeats'],
                                                            week_from=course_time['week_from'],
                                                            week_to=course_time['week_to'])
                    new_course_time.save()
                    if week_from > course_time['week_from']:
                        week_from = course_time['week_from']
                    if week_to < course_time['week_to']:
                        week_to = course_time['week_to']
                # 如果有上课时间信息就可以推断出课程的开始结束周
                if week_from != 100 and week_to:
                    course.week_from = week_from
                    course.week_to = week_to
                    course.save()
                return course


class PublicElectiveCourse(CourseBase):

    COURSE_FIELD_CHOICE = {
        (u'数学与推理类', u'数学与推理类'),
        (u'艺术与欣赏类', u'艺术与欣赏类'),
        (u'人文与社会类', u'人文与社会类'),
        (u'自然与工程类', u'自然与工程类'),
        (u'交流与写作类', u'交流与写作类'),
        (u'研究与领导类', u'研究与领导类'),
        (u'中国与全球类', u'中国与全球类'),
        (u'未知', u'未知'),
        (u'无领域', u'无领域'),
        (u'无', u'无'),
    }

    course_set = models.ForeignKey(verbose_name=u'所属课程集', to=PublicElectiveCourseSet, blank=True, null=True,
                                   related_name='courses')
    field = models.CharField(verbose_name=u'领域', max_length=20, choices=COURSE_FIELD_CHOICE, blank=True, null=True)
    left = models.IntegerField(verbose_name=u'选课剩余人数', default=0, blank=True, null=True)
    total = models.IntegerField(verbose_name=u'课程最大人数', default=0, blank=True, null=True)
    is_popular = models.BooleanField(verbose_name=u'是否为热门课程', default=False)

    # 本节课的所有课的时间 可以通过 (Course_instance).course_time.all() 获得
    # 所有在上这门课的学生 可以通过 (Course_instance).studentcourses.all() 再分别取 .student 获得
    # 所有在选这门课的学生 可以通过 (Course_instance).student_select_this_course_set.all() 获得
    # 所有收藏这门课的学生 可以通过 (Course_instance).student_like_this_course_set.all() 获得

    class Meta:
        verbose_name = u'公选课程'
        verbose_name_plural = u'公选课程'

    @staticmethod
    def get_all_detail_prefetch(query_set):
        query_set = query_set.select_related('teacher', 'academy', 'course_set') \
            .only('number', 'name', 'book', 'week_from', 'week_to', 'credit',
                  'is_open', 'field', 'left', 'total', 'is_popular',
                  'teacher__name', 'academy__name',
                  'course_set__is_check_attendance_top_ten', 'course_set__is_teaching_quality_top_ten',
                  'course_set__is_giving_score_top_ten', 'course_set__is_boy_top_ten', 'course_set__is_girl_top_ten',
                  'course_set__check_attendance_good', 'course_set__check_attendance_medium',
                  'course_set__check_attendance_bad', 'course_set__teaching_quality_good',
                  'course_set__teaching_quality_medium', 'course_set__teaching_quality_bad',
                  'course_set__giving_score_good', 'course_set__giving_score_medium',
                  'course_set__giving_score_bad', 'course_set__summary')
        query_set = query_set.prefetch_related('course_time')
        return query_set

    @staticmethod
    def get_all_detail(query_set):
        """
        调用多个课程详情请用此方法
        可以避免外键查询造成的查询数过多
        :param query_set: QuerySet[PublicElectiveCourse]
        :return:
        """
        query_set = PublicElectiveCourse.get_all_detail_prefetch(query_set)
        result = []
        for query_target in query_set:
            result.append(query_target.get_course_detail())
        return result

    def get_course_detail(self):
        """
        如果data要加值，请查看PublicElectiveCourse.get_all_detail()方法是否要做相应的修改
        :return: dic 课程的详细信息
        """
        all_course_time_set = []
        location_list = []
        location_string = ''
        for course_time in self.course_time.all():
            all_course_time_set.append(course_time.get_course_time_detail())
            if course_time.location and course_time.location not in location_list:
                location_list.append(course_time.location)
                # 用分号分隔开的地点
                location_string += (course_time.location + ';')
        # 去除最后一个分号
        if location_string.endswith(';'):
            location_string = location_string[:-1]
        data = {
            'number': self.number,
            'name': self.name,
            'book': self.book,
            'teacher': self.teacher.name,
            'academy': self.academy.name,
            'week_from': self.week_from,
            'week_to': self.week_to,
            'credit': self.credit,
            'is_open': self.is_open,
            'field': self.field,
            'left': self.left,
            'total': self.total,
            'is_popular': self.is_popular,
            'is_check_attendance_top_ten': self.course_set.is_check_attendance_top_ten,
            'is_teaching_quality_top_ten': self.course_set.is_teaching_quality_top_ten,
            'is_giving_score_top_ten': self.course_set.is_giving_score_top_ten,
            'is_boy_top_ten': self.course_set.is_boy_top_ten,
            'is_girl_top_ten': self.course_set.is_girl_top_ten,
            'time': all_course_time_set,
            'location': location_string,
            'summary': self.course_set.summary,
            'scores': self.get_evaluation(),
        }
        return data

    def get_students_list(self, page=1, per_page=-1):
        data = []
        # 这里查询的是PublicElectiveStudentCourse对应的表
        # 留下student字段是为了接下去的查询，而留下course字段也许是因为django在取回查询结果之后还会对查询条件进行验证
        # 不管取course字段是不是为了验证，反正数据库的log显示不保留course字段就会产生大量取course_id的零散查询
        # 如果get_student_abstract()查询的字段变动，请修改这里的only字段
        student_courses = self.student_courses.exclude(student__name__in=['', u'未知']) \
            .select_related('student').only('course', 'student__name', 'student__gender',
                                            'student__avatar_type_switch', 'student__avatar', 'student__sys_avatar', )
        if per_page == -1:
            pagination = {
                'page': 1,
                'num_pages': 1,
            }
            for student_course in student_courses:
                data.append(student_course.student.get_student_abstract())
            return data, pagination
        else:
            student_course_count = student_courses.count()
            total_pages = student_course_count / per_page
            if student_course_count % per_page:
                total_pages += 1
            if not total_pages:  # 排除总数为0的切片错误
                total_pages = 1
            if page > total_pages:
                page = total_pages
            pagination = {
                'page': page,
                'num_pages': total_pages,
            }
            student_courses_per_page = student_courses[per_page * (page - 1):per_page * page]
            for student_course in student_courses_per_page:
                data.append(student_course.student.get_student_abstract())
            return data, pagination

    @staticmethod
    def course_filter(key_word=None, item_per_page=-1, page=1, weekday=None, location=None, field=None, credit=None,
                      class_time='', has_left=None, order=None):
        """
        筛选课程的函数（注意性能测试啊！！！）
        :param item_per_page: @type: int 每页的数量
        :param page: @type: int 请求的页码
        :param weekday: @type: list 可选的星期
        :param location: @type: list 可选的地点
        :param field: @type: list 可选的领域
        :param credit: @type: list 可选的学分数
        :return: dic:data / string:message, bool: success
        示例data返回：
        {
            'list': [
                {
                    'number': 123,
                },
                {
                    'number': 456,
                },
            ],
            'total_page': 30,
            'current_page': 1,
        }
        """
        # models.Q 对复杂查询有性能加成
        query_filter = models.Q(is_open=True)
        # model.Q 筛选条件的添加顺序为性能考虑，后添加的规则先被应用筛选
        # 附带一个测试实例
        # <Q: (AND: (OR: (AND: ('field1', '1'), ('field2', '2')), ('field3', '3'), ('field4', '4')), ('field5', '5'))>
        # 查看models.Q的添加顺序才特地指定了location筛选条件的添加方式
        if key_word:
            try:
                # 如果输入的是数字
                course_number = int(key_word)
                query_filter &= models.Q(number__contains=course_number)
            except ValueError:
                query_filter &= models.Q(name__icontains=key_word) | models.Q(teacher__name__contains=key_word)
        if location:
            # location_map = {
            #     u'文理学部': u'1',
            #     u'工学部': u'2',
            #     u'信息学部': u'3',
            #     u'医学部': u'4',
            # }
            # 进filter的必须是1/2/3/4
            location_query_filter = models.Q()
            for one_location in location:
                location_query_filter |= models.Q(course_time__location__startswith=one_location)
            query_filter &= location_query_filter
        if credit:
            query_filter &= models.Q(credit__in=credit)
        if weekday:
            query_filter &= models.Q(course_time__weekday__in=weekday)
        # class_time  morning: [1, 5]  afternoon: [6, 10]  evening: [11, 13]
        if class_time:
            if class_time == 'morning':
                query_filter &= models.Q(course_time__class_over__lte=5)
            elif class_time == 'afternoon':
                query_filter &= models.Q(course_time__class_begin__gte=6)
                query_filter &= models.Q(course_time__class_over__lte=10)
            elif class_time == 'evening':
                query_filter &= models.Q(course_time__class_begin__gte=11)
        if field:
            query_filter &= models.Q(field__in=field)
        if has_left:
            query_filter &= ~models.Q(left=0)
        # 因为跨表查询会产生重复的记录，所以distinct()剔除重复的字段
        # 先对queryset切片再迭代可以避免为了取一页的数据跑遍整个数据表
        query_set = PublicElectiveCourse.objects.filter(query_filter).distinct()
        # 进行排序
        # order_by()的条件设置不是增量的，而是会覆盖前面的order设置
        order_key_word_map = {
            'score': '-course_set__giving_score_good',
            'quality': '-course_set__teaching_quality_good',
            'attendance': '-course_set__check_attendance_good',
        }
        real_order = []
        if order:
            for order_key_word in order:
                if order_key_word in order_key_word_map:
                    real_order.append(order_key_word_map[order_key_word])
        query_set = query_set.order_by(*real_order)
        # 确定切片的index值
        if item_per_page == -1:
            total_page = 1
            page = 1
            item_list = PublicElectiveCourse.get_all_detail(query_set)
        else:
            total_item = query_set.count()
            total_page = total_item / int(item_per_page)
            if total_item % int(item_per_page):
                total_page += 1
            if not total_page:  # 排除总数为0的切片错误
                total_page = 1
            if page > total_page:
                page = total_page
            start = item_per_page * (page - 1)
            end = item_per_page * page
            item_list = PublicElectiveCourse.get_all_detail(query_set[start: end])
        data = {
            'current_page': page,
            'total_page': total_page,
            'list': item_list,
        }
        return data

    def get_course_abstract(self):
        """
        对应文档中的PublicElectiveSelectedItem (object) 已选过的公选课信息
        """
        data = {
            'name': self.name,
            'number': self.number,
            'credit': self.credit,
        }
        return data

    def get_evaluation(self):
        data = {
            'attendance': [
                self.course_set.check_attendance_good,
                self.course_set.check_attendance_medium,
                self.course_set.check_attendance_bad,
            ],
            'quality': [
                self.course_set.teaching_quality_good,
                self.course_set.teaching_quality_medium,
                self.course_set.teaching_quality_bad,
            ],
            'score': [
                self.course_set.giving_score_good,
                self.course_set.giving_score_medium,
                self.course_set.giving_score_bad,
            ],
        }
        return data

    def get_other_course_in_the_same_course_set(self):
        all_course_detail = self.course_set.get_all_courses_this_semester()
        index = 0
        for course_detail in all_course_detail:
            if course_detail['number'] == self.number:
                all_course_detail.pop(index)
                break
            index += 1
        return all_course_detail

    def get_students_having_this_course(self):
        """
        :return: list 内嵌dic 所有在上这门课的学生详细信息
        """
        data = []
        for student_course in self.student_courses.all().select_related('student')\
                .only('student__name', 'student__gender', 'course',
                      'student__avatar_type_switch', 'student__avatar', 'student__sys_avatar'):
            data.append(student_course.student.get_student_abstract())
        return data

    def get_student_select_this_course(self):
        """
        :return: list 内嵌dic 所有在选这门课的学生详细信息
        """
        data = []
        for student in self.student_select_this_course.all():
            data.append(student.get_student_abstract())
        return data

    def get_student_like_this_course(self):
        """
        :return: list 内嵌dic 所有收藏这门课的学生详细信息
        """
        data = []
        for student in self.student_like_this_course.all():
            data.append(student.get_student_abstract())
        return data

    def get_all_comment(self, page=1, per_page=-1):
        """
        :return: list 内嵌dic 对应课程集的所有评价详细信息
        """
        return self.course_set.get_all_comment(page, per_page)

    # 经过讨论，用这个学期的男女生人数作参考时效性太差，放弃
    # @staticmethod
    # def get_top_ten(force=None):
    #     # todo:是不是可以直接filter查询？
    #     """
    #     获取男生女生前十
    #     请加入定时任务，并且注意耗时较长
    #     用户访问正常情况下应该是not force模式，直接从缓存中取
    #     force刷新请加入定时任务，并且注意耗时较长
    #     :param force: 是否强制更新
    #     :return: dic: 'top_ten_type':course instance list / string: message, bool: success
    #     """
    #     if not force:
    #         data = cache.get('top_ten_course')
    #         if data:
    #             return data, True
    #         else:
    #             # 用户访问为not force，但是没有取到缓存，直接返回空，打critical日志
    #             # 用户是没有耐心来等服务器重新跑一遍数据库拿到新鲜的数据的
    #             # todo：出现这种错误是否可以直接开启一个后台的任务来force刷新一遍
    #             # todo：特别是服务器重启了过后是不是应该自动刷新一次
    #             logger.critical(u'top_ten_course_set cache fetch failed, plz refresh')
    #             return 11, False
    #     else:
    #         # 遍历数据库刷新排行榜
    #         # 如果没有正常执行完成打critical日志
    #         try:
    #             # 先把所有top_ten数据清空
    #             for course in PublicElectiveCourse.objects.filter(models.Q(is_boy_top_ten=True) |
    #                                                               models.Q(is_girl_top_ten=True)):
    #                 course.is_boy_top_ten = False
    #                 course.is_girl_top_ten = False
    #                 course.save()
    #             print 'data clearance complete'
    #             # 再开始计算
    #             boy_top_ten = []
    #             girl_top_ten = []
    #             for course in PublicElectiveCourse.objects.filter(is_open=True):
    #                 total_student = course.student_courses.all().count()
    #                 boy_count = course.student_courses.filter(student__gender=u'男').count()
    #                 girl_count = course.student_courses.filter(student__gender=u'女').count()
    #                 # 根据设置的最少人数，最少男生数，最少女生数进行筛选
    #                 can_join_boy_top_ten = False
    #                 can_join_girl_top_ten = False
    #                 if total_student < settings.TOP_TEN_FILTER['MIN_STUDENT']:
    #                     # 人数不足跳出计算
    #                     continue
    #                 if boy_count > settings.TOP_TEN_FILTER['MIN_BOY']:
    #                     # 满足加入男生前十的条件
    #                     can_join_boy_top_ten = True
    #                 if girl_count > settings.TOP_TEN_FILTER['MIN_GIRL']:
    #                     # 满足加入女生前十的条件
    #                     can_join_girl_top_ten = True
    #                 course.boy_percentage = float(boy_count) / float(total_student)
    #                 course.girl_percentage = float(girl_count) / float(total_student)
    #                 # 将课程按照男生比例从高到低依次加入top ten的列表
    #                 if can_join_boy_top_ten:
    #                     if boy_top_ten:
    #                         if len(boy_top_ten) < 10:
    #                             # 如果前十的排名不为空，并且数量没有到达十个
    #                             # 从高到低查找本次计算的课程按照男生比例应该插入到哪个位置并插入
    #                             i = 0
    #                             is_inserted = False
    #                             for high_percentage_course in boy_top_ten:
    #                                 if high_percentage_course.boy_percentage < course.boy_percentage:
    #                                     boy_top_ten.insert(i, course)
    #                                     is_inserted = True
    #                                     break
    #                                 i += 1
    #                             if not is_inserted:
    #                                 boy_top_ten.append(course)
    #                         else:
    #                             # 如果前十的排名不为空，并且数量没有到达十个
    #                             # 从高到低查找本次计算的课程按照男生比例应该插入到哪个位置并插入
    #                             # 最后弹出最后一个课程
    #                             i = 0
    #                             for high_percentage_course in boy_top_ten:
    #                                 if high_percentage_course.boy_percentage < course.boy_percentage:
    #                                     boy_top_ten.insert(i, course)
    #                                     boy_top_ten.pop()
    #                                     break
    #                                 i += 1
    #                     else:
    #                         # 如果前十的排名为空，直接加入
    #                         boy_top_ten.append(course)
    #
    #                 if can_join_girl_top_ten:
    #                     if girl_top_ten:
    #                         if len(girl_top_ten) < 10:
    #                             i = 0
    #                             is_inserted = False
    #                             for high_percentage_course in girl_top_ten:
    #                                 if high_percentage_course.girl_percentage < course.girl_percentage:
    #                                     girl_top_ten.insert(i, course)
    #                                     is_inserted = True
    #                                     break
    #                                 i += 1
    #                             if not is_inserted:
    #                                 girl_top_ten.append(course)
    #                         else:
    #                             i = 0
    #                             for high_percentage_course in girl_top_ten:
    #                                 if high_percentage_course.girl_percentage < course.girl_percentage:
    #                                     girl_top_ten.insert(i, course)
    #                                     girl_top_ten.pop()
    #                                     break
    #                                 i += 1
    #                     else:
    #                         girl_top_ten.append(course)
    #             print 'calculation complete'
    #             # 赋值
    #             boy_top_ten_detail = []
    #             girl_top_ten_detail = []
    #             for course in boy_top_ten:
    #                 course.is_boy_top_ten = True
    #                 course.save()
    #                 boy_top_ten_detail.append(course.get_course_detail())
    #             for course in girl_top_ten:
    #                 course.is_girl_top_ten = True
    #                 course.save()
    #                 girl_top_ten_detail.append(course.get_course_detail())
    #             data = {
    #                 'boy': boy_top_ten_detail,
    #                 'girl': girl_top_ten_detail,
    #             }
    #             # 存入缓存
    #             cache.set('top_ten_course', data, None)
    #             return data, True
    #         except Exception, error:
    #             logger.critical(u'%s, failed to refresh top ten course,'
    #                             u' please debug and refresh again' % error.message)
    #             return '%s, failed to refresh top ten course set, ' \
    #                    'please debug and refresh again' % error.message, False


class SpecializedCourse(CourseBase):

    TYPE_CHOICE = {
        (u'专业必修', u'专业必修'),
        (u'专业选修', u'专业选修'),
    }

    type = models.CharField(verbose_name=u'课程类型', max_length=5, choices=TYPE_CHOICE, blank=True, null=True)
    course_set = models.ForeignKey(verbose_name=u'所属课程集', to=SpecializedCourseSet, blank=True, null=True,
                                   related_name='courses')
    major = models.CharField(verbose_name=u'专业名称', max_length=30, blank=True, null=True)
    grade = models.IntegerField(verbose_name=u'年级', blank=True, null=True)

    class Meta:
        verbose_name = u'专业课程'
        verbose_name_plural = u'专业课程'

    # Student.recommend_specialized_course() 推荐专业课
    # Student.delete_recommended_specialized_course() 删除推荐专业课

    @staticmethod
    def get_all_detail(query_set):
        """
        调用多个课程详情请用此方法
        可以避免外键查询造成的查询数过多
        :param query_set: QuerySet[SpecializedCourse]
        :return:
        """
        query_set = query_set.select_related('teacher', 'academy', 'course_set')\
            .only('number', 'name', 'book', 'week_from', 'week_to', 'credit', 'is_open', 'type', 'major', 'grade',
                  'academy__name', 'teacher__name', 'course_set__star')
        query_set = query_set.prefetch_related('course_time')
        result = []
        for query_target in query_set:
            result.append(query_target.get_course_detail())
        return result

    def get_course_detail(self):
        all_course_time_set = []
        location_list = []
        location_string = ''
        for course_time in self.course_time.all():
            all_course_time_set.append(course_time.get_course_time_detail())
            if course_time.location not in location_list:
                location_list.append(course_time.location)
                # 用分号分隔开的地点
                location_string += (course_time.location + ';')
        # 去除最后一个分号
        if location_string.endswith(';'):
            location_string = location_string[:-1]
        data = {
            'number': self.number,
            'name': self.name,
            'book': self.book,
            'teacher': self.teacher.name,
            'academy': self.academy.name,
            'week_from': self.week_from,
            'week_to': self.week_to,
            'credit': self.credit,
            'is_open': self.is_open,
            'type': self.type,
            'major': self.major,
            'grade': self.grade,
            'time': all_course_time_set,
            'location': location_string,
            'star': self.course_set.star,
        }
        return data

    @staticmethod
    def get_course_list(academy=None, per_page=-1, page=1):
        if academy:
            query_filter = models.Q(academy__name=academy) & models.Q(is_open=True)
        else:
            query_filter = models.Q(is_open=True)
        query_set = SpecializedCourse.objects.filter(query_filter).order_by('-course_set__star')

        if per_page == -1:
            pagination = {
                'page': 1,
                'num_page': 1,
            }
            detail_list = SpecializedCourse.get_all_detail(query_set)
        else:
            total_page = query_set.count() / per_page
            if query_set.count() % per_page:
                total_page += 1
            if not total_page:  # 排除总数为0的切片错误
                total_page = 1
            if page > total_page:
                page = total_page
            pagination = {
                'page': page,
                'num_page': total_page,
            }
            start = (page - 1) * per_page
            end = page * per_page
            detail_list = SpecializedCourse.get_all_detail(query_set[start: end])
        return detail_list, pagination


class PublicCompulsoryCourse(CourseBase):
    course_set = models.ForeignKey(verbose_name=u'所属课程集', to=PublicCompulsoryCourseSet, blank=True, null=True,
                                   related_name='courses')

    class Meta:
        verbose_name = u'公必课程'
        verbose_name_plural = u'公必课程'

    @staticmethod
    def get_all_detail(query_set):
        """
        调用多个课程详情请用此方法
        可以避免外键查询造成的查询数过多
        :param query_set: QuerySet[PublicCompulsoryCourse]
        :return:
        """
        query_set = query_set.select_related('teacher', 'academy', 'course_set') \
            .only('number', 'name', 'book', 'week_from', 'week_to', 'credit', 'is_open',
                  'academy__name', 'teacher__name', 'course_set__summary')
        query_set = query_set.prefetch_related('course_time')
        result = []
        for query_target in query_set:
            result.append(query_target.get_course_detail())
        return result

    def get_course_detail(self):
        all_course_time_set = []
        location_list = []
        location_string = ''
        for course_time in self.course_time.all():
            all_course_time_set.append(course_time.get_course_time_detail())
            if course_time.location not in location_list:
                location_list.append(course_time.location)
                # 用分号分隔开的地点
                location_string += (course_time.location + ';')
        # 去除最后一个分号
        if location_string.endswith(';'):
            location_string = location_string[:-1]
        data = {
            'number': self.number,
            'name': self.name,
            'book': self.book,
            'teacher': self.teacher.name,
            'academy': self.academy.name,
            'week_from': self.week_from,
            'week_to': self.week_to,
            'credit': self.credit,
            'is_open': self.is_open,
            'time': all_course_time_set,
            'location': location_string,
            'summary': self.course_set.summary,
        }
        return data


class CourseTimeBase(models.Model):  # 某一课程的单次课的时间信息
    location = models.TextField(verbose_name=u'上课地点', blank=True, null=True)
    class_begin = models.IntegerField(verbose_name=u'从x节课开始', blank=True, null=True)
    class_over = models.IntegerField(verbose_name=u'到x节课结束', blank=True, null=True)
    weekday = models.IntegerField(verbose_name=u'星期x上课', blank=True, null=True)
    repeat = models.IntegerField(verbose_name=u'每x周上一次课', blank=True, null=True)
    week_from = models.IntegerField(verbose_name=u'从x周开始', blank=True, null=True)
    week_to = models.IntegerField(verbose_name=u'到x周结束', blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name = u'单次课的时间信息'
        verbose_name_plural = u'单次课的时间信息'

    def __unicode__(self):
        return u'%s的时间信息' % self.course

    def get_course_time_detail(self):
        """
        :return: dic 课程时间的详细信息
        """
        data = {
            'location': self.location,
            'class_begin': self.class_begin,
            'class_over': self.class_over,
            'weekday': self.weekday,
            'repeat': self.repeat,
            'week_from': self.week_from,
            'week_to': self.week_to,
        }
        return data


class PublicElectiveCourseTime(CourseTimeBase):
    course = models.ForeignKey(verbose_name=u'对应课程', to=PublicElectiveCourse, blank=True, null=True,
                               related_name='course_time')

    class Meta:
        verbose_name = u'公选课时间信息'
        verbose_name_plural = u'公选课时间信息'

    def __unicode__(self):
        return u'%s的时间信息' % self.course


class SpecializedCourseTime(CourseTimeBase):
    course = models.ForeignKey(verbose_name=u'对应课程', to=SpecializedCourse, blank=True, null=True,
                               related_name='course_time')

    class Meta:
        verbose_name = u'专业课的时间信息'
        verbose_name_plural = u'专业课的时间信息'

    def __unicode__(self):
        return u'%s的时间信息' % self.course


class PublicCompulsoryCourseTime(CourseTimeBase):
    course = models.ForeignKey(verbose_name=u'对应课程', to=PublicCompulsoryCourse, blank=True, null=True,
                               related_name='course_time')

    class Meta:
        verbose_name = u'体育课的时间信息'
        verbose_name_plural = u'体育课的时间信息'

    def __unicode__(self):
        return u'%s的时间信息' % self.course


class PopularPublicElectiveCourse(models.Model):
    # 将此表独立的理由
    # 1. 取用数据查询成本降低，并且查询方式单一
    # 2. 此表关系简单，外键很少，编码逻辑简单
    # 3. 此表是由运维人员手动从admin后台录入，单独建表要是玩坏了修复较快，目测不能让运维人员修改Course主表
    # 4. 数据库里存的是一个课程的外键，但是同步的is_popular字段是course_set里面的和所有相关的在同一课程集的课程
    #    因为直接让运维去找一个course_set和下面所有的课程比较反人类
    # 5. 取用热门课程的数据的时候也正好只能任意先取一个，正好就用运维输入的那个咯
    #    当然通过课程详情页应该也能找到其他可能的选择
    course = models.ForeignKey(verbose_name=u'对应课程', to=PublicElectiveCourse, blank=True, null=True)

    class Meta:
        verbose_name = u'热门公选课程'
        verbose_name_plural = u'热门公选课程'

    def __unicode__(self):
        return u'%s的热门课程' % self.course

    def save(self, *args, **kwargs):
        """
        将对应的课程集的is_popular字段置为True
        同时将所有同一课程集内的相应的课程is_popular字段置为True
        """
        super(PopularPublicElectiveCourse, self).save(*args, **kwargs)
        course_set = self.course.course_set
        course_set.is_popular = True
        course_set.save()
        for course in course_set.courses.filter(is_open=True):
            # course_set为PublicElectiveCourseSet实例，courses为ManyToManyField查询
            # 这里的course为Course实例
            course.is_popular = True
            course.save()

    def delete(self, using=None):
        """
        将对应的课程集的is_popular字段置为False
        同时将所有同一课程集内的相应的课程is_popular字段置为False
        """
        course_set = self.course.course_set
        course_set.is_popular = False
        course_set.save()
        for course in course_set.courses.all():
            # course_set为PublicElectiveCourseSet实例，courses为ManyToManyField查询
            # 这里的course为Course实例
            course.is_popular = False
            course.save()
        super(PopularPublicElectiveCourse, self).delete()

    @staticmethod
    @static_data_cache
    def get_all_popular_course_with_other_course_option():
        """
        返回所有的热门课程详情
        顺便还有其他的时间同课程的选择
        :return:
        """
        query_set = PopularPublicElectiveCourse.objects.all()\
            .select_related('course', 'course__course_set', 'course__academy', 'course__teacher')\
            .prefetch_related('course__course_time', 'course__course_set__courses',
                              'course__course_set__courses__academy',
                              'course__course_set__courses__teacher',
                              'course__course_set__courses__course_time')
        popular_course_list = []
        for popular_course in query_set:
            detail = popular_course.course.get_course_detail()
            # 不直接用这个是因为调用这个会造成多余的select_related和prefetch_related
            # detail['other_courses'] = popular_course.course.get_other_course_in_the_same_course_set()
            other_courses_list = []
            for other_course in popular_course.course.course_set.courses.all():
                if other_course.number != popular_course.course.number:
                    other_courses_list.append(other_course.get_course_detail())
            detail['other_courses'] = other_courses_list
            popular_course_list.append(detail)
        return popular_course_list

