# -*- coding: utf-8 -*-
import logging

__author__ = 'fuxiuyin'

logger = logging.getLogger('api_error')


class DevError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value.encode('utf-8')
#
#
# class ErrorCodeManager(object):
#
#     def __init__(self):
#         self.code_to_message = {}
#
#     def __setitem__(self, key, value):
#         old_error = self.code_to_message.get(key)
#         if old_error:
#             raise DevError(u"error %s 和 error %s 定义了同样的 error_code %s" % (value, old_error, key))
#         self.code_to_message.setdefault(key, value)
#
#     def __getitem__(self, item):
#         """
#         :param item:
#         @type item: int
#         :return:
#         """
#         return self.code_to_message[item]
#
# error_code_manager = ErrorCodeManager()
#
#
# class APIErrorMeta(type):
#
#     def __new__(cls, class_name, bases, attrs):
#         if class_name == "APIError":
#             return super(APIErrorMeta, cls).__new__(cls, class_name, bases, attrs)
#         error_code = attrs.get('error_code')
#         if not error_code:
#             raise DevError(u"\n error:%s 没有定义error_code" % class_name)
#         error_class = super(APIErrorMeta, cls).__new__(cls, class_name, bases, attrs)
#         error_code_manager[error_code] = error_class
#
#         return error_class


class APIError(Exception):

    error_code = None
    message = 'unknown error'

    # __metaclass__ = APIErrorMeta

    def __init__(self, message, *args, **kwargs):
        self.message = message
        logger.error(u'%s %s' % (self.error_code, message))
        super(APIError, self).__init__(*args, **kwargs)

    # def code(self):
    #     return self.error_code

    def __str__(self):
        return self.message.encode('utf-8')
