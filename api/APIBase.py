# -*- coding: utf-8 -*-
import requests
import hmac
import struct
import hashlib
import time
import base64
import random
import logging

from APIErrors import *
from APIErrorBase import DevError

__author__ = 'fuxiuyin'

requests.packages.urllib3.disable_warnings()

logger = logging.getLogger('api_info')


class APIMeta(type):
    def __new__(cls, class_name, bases, attrs):
        if class_name == "APIBase":
            return super(APIMeta, cls).__new__(cls, class_name, bases, attrs)
        url = attrs.get('url')
        if not url:
            raise DevError(u"%s没有指定接口地址,请定义url参数" % class_name)
        method = attrs.get('method')
        if method == 'get':
            attrs['method'] = APIBase.get
        elif method == 'post':
            attrs['method'] = APIBase.post
        elif method == 'put':
            attrs['method'] = APIBase.put
        else:
            raise DevError(u"%s没有指定method方法，或指定的method不为('get', 'post', 'put'中的一个")

        attrs['url'] = APIBase.api_url + url
        return super(APIMeta, cls).__new__(cls, class_name, bases, attrs)


class APIBase(object):
    __metaclass__ = APIMeta

    api_url = "https://api.ziqiang.net.cn"
    url = None
    method = None
    result_key = None

    def __call__(self, student):
        header = self.get_auth_header()
        header.update(self.pre_header(student))
        data = self.pre_data(student)
        response = self.method(student, header, data)
        if response.content.decode('utf-8').__contains__(u'\ufffd'):
            raise APINotAvailable
        result = self.get_result(response, self.result_key)
        logger.info(u'%s %s %s %s' % (self.url, student.number, data, result))
        return result

    def pre_header(self, student):
        return {}

    @staticmethod
    def get_token_header(student):
        if student.token and student.rf_token:
            return {'Authorization': 'Token %s' % student.token}
        else:
            raise InvalidToken(student)

    # 加入掌上武大auth请求头
    def get_auth_header(self):
        nonce = random.randint(0, 1000000000)
        time_now = int(time.time())
        signature = self.generate_signature(self.get_secret_key(), time_now, nonce)
        return {'x-zswd-signature': signature, 'x-zswd-timestamp': time_now, 'x-zswd-nonce': nonce}

    def pre_data(self, student):
        return None

    @staticmethod
    def get_result(response, keys=None):
        """

        :param response:
        @type response: requests.models.Response
        :param keys:
        @type keys: list
        :return:
        """
        try:
            result = response.json()
        except ValueError:
            raise APINotAvailable(response)
        if keys:
            results = {}
            for key in keys:
                results.setdefault(key, result.get(key))
            return results
        else:
            return result

    @staticmethod
    def error_handler(response, student):
        error_message = APIBase.get_result(response, keys=['message'])['message']
        if response.status_code == 401:
            if error_message == 'token has expired':
                raise TokenExpireError(student)
            else:
                raise InvalidToken(student)
        elif response.status_code == 403:
            raise PermissionError(student, "edu")
        elif response.status_code == 400:
            if error_message.startswith("educational system pass"):
                raise EduPwdError(student)
            elif error_message.startswith("educational system cap"):
                raise CaptchaError()
            elif error_message.startswith("educational system is"):
                raise EduSysError()
            else:
                raise UnknownError()

        errors = APIBase.get_result(response, keys=['errors'])['errors']
        if errors.get("sid"):
            raise InvalidStudentId(student)
        non_field_errors = errors.get("non_field_errors")
        if non_field_errors:
            for error_message in non_field_errors:
                if error_message.startswith('education captcha error'):
                    raise CaptchaError()
                elif error_message.startswith("edu"):
                    raise EduPwdError(student)
                elif error_message.startswith("inv"):
                    raise InvalidToken(student)

    def post(self, student, header=None, data=None):
        """
        :param student: not use but used in error handle
        :param header:
        :param data:
        :return:
        """
        r = requests.post(self.url, headers=header, data=data)
        if r.status_code == 200:
            return r
        else:
            APIBase.error_handler(r, student)

    def put(self, student, header=None, body=None):
        """
        :param student: not use but used in error handle
        :param header:
        :param body:
        :return:
        """
        r = requests.put(self.url, headers=header, data=body)
        if r.status_code == 200:
            return r
        else:
            APIBase.error_handler(r, student)

    def get(self, student, header=None, data=None):
        """

        :param student: not use but used in error handle
        :param header:
        :param data:
        :return:
        """
        r = requests.get(self.url, headers=header, params=data)
        if r.status_code == 200:
            return r
        else:
            APIBase.error_handler(r, student)

    # 处理掌武api的请求合法性验证
    @staticmethod
    def generate_signature(secret_key, timestamp, nonce):
        secret_key = str(secret_key)
        timestamp = str(timestamp)
        nonce = str(nonce)
        tmp_list = [secret_key, timestamp, nonce]
        tmp_list.sort()
        tmp_str = ''.join(tmp_list)
        return hashlib.sha1(tmp_str).hexdigest()

    # 处理掌武api的请求合法性验证
    @staticmethod
    def hotp(secret, number):
        key = base64.b32decode(secret, True)
        msg = struct.pack(">Q", number)
        digest = hmac.new(key, msg, hashlib.sha1).digest()
        offset = ord(digest[19]) & 15
        token_base = digest[offset:offset + 4]
        token = (struct.unpack(">I", token_base)[0] & 0x7fffffff)
        return token

    # 处理掌武api的请求合法性验证
    def totp(self, secret):
        return self.hotp(secret, number=int(time.time()) // 43200)

    # 处理掌武api的请求合法性验证
    def get_secret_key(self):
        totp_secret_key = '+.a*vD@JrLYauz(M)HA^=}sh'
        return self.totp(base64.b32encode(totp_secret_key))
