# -*- coding: utf-8 -*-
from APIBase import APIBase
from APIErrors import InvalidToken
__author__ = 'fuxiuyin'


class GetTokenAPI(APIBase):
    method = 'get'
    url = '/token/'
    result_key = ['token', 'refresh_token', 'expires_in']

    def pre_data(self, student):
        return {'sid': student.number, 'binding_type': 3, 'pwd_edu': student.password, 'check': 1}


class RefreshTokenAPI(APIBase):
    method = 'put'
    url = '/token/'
    result_key = ['token', 'refresh_token', 'expires_in']

    def pre_data(self, student):
        if student.token and student.rf_token:
            return {'token': student.token, 'refresh_token': student.rf_token}
        else:
            raise InvalidToken(student)


class CheckTokenStatusAPI(APIBase):
    method = 'get'
    url = '/token/'
    result_key = ['expires_in']

    def pre_data(self, student):
        if student.token and student.rf_token:
            return {'token': student.token}
        else:
            raise InvalidToken(student)


class ChangePwdAPI(APIBase):
    method = 'put'
    url = '/token/update_permissions/'
    result_key = ['token', 'refresh_token', 'expires_in']

    def pre_data(self, student):
        if student.token and student.rf_token:
            return {'token': student.token, 'refresh_token': student.rf_token, 'pwd_edu': student.password, 'check': 1}
        else:
            raise InvalidToken(student)


class GetCourseAPI(APIBase):
    method = 'get'
    url = '/courses/'
    result_key = None

    def pre_data(self, student):
        return {'update': 1}

    def pre_header(self, student):
        return self.get_token_header(student)


class GetGpaAPI(APIBase):
    method = 'get'
    url = '/courses/gpa/normal/'
    result_key = ['gpa']

    def pre_data(self, student):
        return {'update': 1, 'need_detail': 0}

    def pre_header(self, student):
        return self.get_token_header(student)


class GetGenderAPI(APIBase):
    method = 'get'
    url = '/information/extend/'
    result_key = ['gender']

    def pre_data(self, student):
        return {'update': 1}

    def pre_header(self, student):
        return self.get_token_header(student)


class GetInformationAPI(APIBase):
    method = 'get'
    url = '/information/basic/'
    result_key = ['name', 'college']

    def pre_data(self, student):
        return {'update': 1}

    def pre_header(self, student):
        return self.get_token_header(student)


class GetPublicElectiveCourseDetailAPI(APIBase):
    method = 'get'
    url = '/elective_field/public_elective/'

    def pre_header(self, student):
        return self.get_token_header(student)


class GetPublicElectiveCourseSelectionResultAPI(APIBase):
    method = 'get'
    url = '/selection/public_elective/'

    def pre_header(self, student):
        return self.get_token_header(student)


class SelectPublicSelectiveCourseAPI(APIBase):
    method = 'post'
    url = '/selection/public_elective/'

    def pre_header(self, student):
        return self.get_token_header(student)

    def pre_data(self, student):
        data = {'lessons': ''}
        # 取出传入的临时选课列表
        for course_number in student.temp_selection_course_number_list:
            data['lessons'] += (str(course_number) + ',')
        # 删去最后一个逗号
        if data['lessons'].endswith(','):
            data['lessons'] = data['lessons'][:-1]
        return data
