# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.views.decorators.csrf import ensure_csrf_cookie


@ensure_csrf_cookie
def pc_home_page(request):
    return render_to_response('pc.html')


@ensure_csrf_cookie
def webapp_home_page(request):
    return render_to_response('webapp.html')
